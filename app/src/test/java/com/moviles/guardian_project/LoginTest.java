package com.moviles.guardian_project;

import com.moviles.guardian_project.activities.login.Login2;
import com.moviles.guardian_project.entities.UsuarioProtegido;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class LoginTest {

    @Test
    public void testVerificacionCorreo() {

        String correo="juan@hotmail.com";
        String contraseña = "dsfds545";
        String correo2="juhotmail.com";
        String contraseña2 = "754";

        Login2 login = new Login2();


        assertEquals("No se verifica correctamente el correo", login.isEmailValid(correo), true);
        assertEquals("No se verifica correctamente el correo", login.isEmailValid(correo2), false);
        assertEquals("No se verifica correctamente la contraseña", login.isPasswordValid(contraseña), true);
        assertEquals("No se verifica correctamente la contraseña", login.isPasswordValid(contraseña2), false);



    }



}