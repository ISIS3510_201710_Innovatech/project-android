package com.moviles.guardian_project;

import com.moviles.guardian_project.entities.UsuarioProtegido;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Map;
import static android.R.attr.value;

public class UsuarioProtegidoTest {

    @Test
    public void testConvertHashMap() {
        String id ="htrihfs454dsf";
        String nombre="Carlos";
        String telefono  ="478656";
        String correo="carlos@hotmail.com";
        String direccion = "cll 241# 12 a 43";
        String estado = "En peligro";


        UsuarioProtegido prueba= new  UsuarioProtegido( id,  nombre,  telefono,  correo,  direccion, estado);

        Map<String, Object> mapPrueba = prueba.toMap();

        assertEquals("Conversion toMap del id falló", id, mapPrueba.get("id"));
        assertEquals("Conversion toMap del nombre falló", nombre, mapPrueba.get("nombre"));
        assertEquals("Conversion toMap del teléfono falló", telefono, mapPrueba.get("telefono"));
        assertEquals("Conversion toMapdel correo  falló", correo, mapPrueba.get("correo"));
        assertEquals("Conversion toMap de la dirección falló", direccion, mapPrueba.get("direccion"));
        assertEquals("Conversion toMap del estado falló", estado, mapPrueba.get("estado"));



    }



}