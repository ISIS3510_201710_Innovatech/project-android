package com.moviles.guardian_project;

import com.moviles.guardian_project.entities.Solicitud;
import com.moviles.guardian_project.entities.UsuarioProtegido;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.junit.Assert.assertEquals;

public class SolicitudTest {

    @Test
    public void testConvertHashMap() {
        String id ="jisdkaj";
        String nombre="Juan";
        String telefono  ="jisdkaj";
        String correo="juan@hotmail.com";
        String direccion = "cll falsa 123";

         String de=id;
         String para="dsfdsfg";
         String tipo="Enviar";
         String paraCorreo="pepito@hotrmail.com";
         String paraNombre="Pepito";
         String deCorreo="juan@hotmail.com";
         String deNombre="Juan";


        HashMap<String, Object> infoContacto = new HashMap<String, Object>();
        infoContacto.put("nombre", nombre);
        infoContacto.put("correo", correo);
        infoContacto.put("telefono", telefono);
        infoContacto.put("direccion", direccion);

        Solicitud prueba= new Solicitud( de,  para,  tipo, infoContacto, paraCorreo, paraNombre, deCorreo, deNombre) ;


        Map<String, Object> mapPrueba = prueba.toMap();

        assertEquals("Conversion toMap falló en DE ", de, mapPrueba.get("de"));
        assertEquals("Conversion toMap falló en PARA", para, mapPrueba.get("para"));
        assertEquals("Conversion toMap falló en TIPO", tipo, mapPrueba.get("tipo"));
        assertEquals("Conversion toMap falló en INFOCONTACTO", infoContacto, mapPrueba.get("infoContacto"));
        assertEquals("Conversion toMap falló en PARA OORREO", paraCorreo, mapPrueba.get("paraCorreo"));
        assertEquals("Conversion toMap falló en PARA NOMBRE" , paraNombre, mapPrueba.get("paraNombre"));
        assertEquals("Conversion toMap falló en DE CORREO", deCorreo, mapPrueba.get("deCorreo"));
        assertEquals("Conversion toMap falló en DE NOMBRE", deNombre, mapPrueba.get("deNombre"));

  }



}