package com.moviles.guardian_project.activities.login;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.moviles.guardian_project.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class DrawerU1Test {

    @Rule
    public ActivityTestRule<Login2> mActivityTestRule = new ActivityTestRule<>(Login2.class);

    @Test
    public void drawerU1Test() throws InterruptedException {
        ViewInteraction appCompatAutoCompleteTextView = onView(
                withId(R.id.email));
        appCompatAutoCompleteTextView.perform(scrollTo(), replaceText("usuario1@1.com"), closeSoftKeyboard());

        Thread.sleep(2000);
        ViewInteraction appCompatEditText = onView(
                withId(R.id.password));
        appCompatEditText.perform(scrollTo(), replaceText("usuario1"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.login_correo2), withText("Conectarse"),
                        withParent(allOf(withId(R.id.email_login_form),
                                withParent(withId(R.id.login_form))))));
        appCompatButton.perform(scrollTo(), click());
        Thread.sleep(2000);
        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction appCompatCheckedTextView = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Inicio"), isDisplayed()));
        appCompatCheckedTextView.perform(click());

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton2.perform(click());

        ViewInteraction appCompatCheckedTextView2 = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Emergencias reportadas"), isDisplayed()));
        appCompatCheckedTextView2.perform(click());
        Thread.sleep(9000);
        ViewInteraction textView = onView(
                allOf(withId(android.R.id.text1), withText("Robo:  Mayo 3 de 2013"),
                        childAtPosition(
                                allOf(withId(R.id.lista_reportadas),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.RelativeLayout.class),
                                                0)),
                                0),
                        isDisplayed()));
        textView.check(matches(isDisplayed()));
        Thread.sleep(2000);
        ViewInteraction appCompatImageButton3 = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton3.perform(click());

        ViewInteraction appCompatCheckedTextView3 = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Emergencias atendidas"), isDisplayed()));
        appCompatCheckedTextView3.perform(click());
        Thread.sleep(9000);
        ViewInteraction textView2 = onView(
                allOf(withId(android.R.id.text1), withText("Robos:                     4"),
                        childAtPosition(
                                allOf(withId(R.id.lista_atendidas),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.RelativeLayout.class),
                                                0)),
                                0),
                        isDisplayed()));
        textView2.check(matches(isDisplayed()));
        Thread.sleep(2000);
        ViewInteraction appCompatImageButton4 = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton4.perform(click());

        ViewInteraction appCompatCheckedTextView4 = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Ver lista de protegidos"), isDisplayed()));
        appCompatCheckedTextView4.perform(click());
        Thread.sleep(9000);
        ViewInteraction view = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.lista_protegidos),
                                0),
                        0),
                        isDisplayed()));
        view.check(matches(isDisplayed()));
        Thread.sleep(2000);
        ViewInteraction appCompatImageButton5 = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton5.perform(click());

        ViewInteraction appCompatCheckedTextView5 = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Ver lista de protectores"), isDisplayed()));
        appCompatCheckedTextView5.perform(click());
        Thread.sleep(9000);
        ViewInteraction view2 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.lista_protectores),
                                0),
                        0),
                        isDisplayed()));
        view2.check(matches(isDisplayed()));
        Thread.sleep(2000);
        ViewInteraction appCompatImageButton6 = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton6.perform(click());

        ViewInteraction appCompatCheckedTextView6 = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Numeros de emergencia"), isDisplayed()));
        appCompatCheckedTextView6.perform(click());
        Thread.sleep(9000);
        ViewInteraction view3 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.lista_numeros),
                                0),
                        0),
                        isDisplayed()));
        view3.check(matches(isDisplayed()));
        Thread.sleep(2000);
        ViewInteraction appCompatImageButton7 = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton7.perform(click());

        ViewInteraction appCompatCheckedTextView7 = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Solicitudes recibidas"), isDisplayed()));
        appCompatCheckedTextView7.perform(click());
        Thread.sleep(9000);
        ViewInteraction view4 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.lista_mias_solicitudes),
                                0),
                        0),
                        isDisplayed()));
        view4.check(matches(isDisplayed()));

        ViewInteraction imageView = onView(
                allOf(withId(R.id.iconsolic_mias),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.RelativeLayout.class),
                                        0),
                                0),
                        isDisplayed()));
        Thread.sleep(2000);
        ViewInteraction appCompatImageButton8 = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton8.perform(click());

        ViewInteraction appCompatCheckedTextView8 = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Solicitudes enviadas"), isDisplayed()));
        appCompatCheckedTextView8.perform(click());
        Thread.sleep(9000);
        ViewInteraction textView3 = onView(
                allOf(withText("Enviadas"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.view.View.class),
                                                2)),
                                1),
                        isDisplayed()));
        Thread.sleep(9000);
        ViewInteraction view5 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.lista_sol_realic),
                                0),
                        0),
                        isDisplayed()));
        view5.check(matches(isDisplayed()));
        Thread.sleep(2000);
        ViewInteraction appCompatImageButton9 = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton9.perform(click());

        ViewInteraction appCompatCheckedTextView9 = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Numeros de emergencia"), isDisplayed()));
        appCompatCheckedTextView9.perform(click());
        Thread.sleep(9000);
        ViewInteraction imageView2 = onView(
                allOf(withId(R.id.imageView4),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.RelativeLayout.class),
                                        0),
                                0),
                        isDisplayed()));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
