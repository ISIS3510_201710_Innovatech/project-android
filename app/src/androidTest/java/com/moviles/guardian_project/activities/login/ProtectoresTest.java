package com.moviles.guardian_project.activities.login;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.moviles.guardian_project.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ProtectoresTest {

    @Rule
    public ActivityTestRule<Login2> mActivityTestRule = new ActivityTestRule<>(Login2.class);

    @Test
    public void protectoresTest() throws InterruptedException {
        ViewInteraction appCompatAutoCompleteTextView = onView(
                withId(R.id.email));
        Thread.sleep(5000);
        appCompatAutoCompleteTextView.perform(scrollTo(), replaceText("usuario1@1.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText = onView(
                withId(R.id.password));
        appCompatEditText.perform(scrollTo(), replaceText("usuario1"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.login_correo2), withText("Conectarse"),
                        withParent(allOf(withId(R.id.email_login_form),
                                withParent(withId(R.id.login_form))))));
        appCompatButton.perform(scrollTo(), click());

        Thread.sleep(5000);
        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction appCompatCheckedTextView = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Emergencias reportadas"), isDisplayed()));
        appCompatCheckedTextView.perform(click());

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton2.perform(click());

        Thread.sleep(5000);
        ViewInteraction appCompatCheckedTextView2 = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Ver lista de protectores"), isDisplayed()));
        appCompatCheckedTextView2.perform(click());

        Thread.sleep(5000);
        ViewInteraction view = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.lista_protectores),
                                0),
                        0),
                        isDisplayed()));
        view.check(matches(isDisplayed()));

        ViewInteraction view2 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.lista_protectores),
                                1),
                        0),
                        isDisplayed()));
        view2.check(matches(isDisplayed()));

        ViewInteraction relativeLayout = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.lista_protectores),
                                withParent(withId(R.id.activity_protectores_swipe_refresh_layout))),
                        0),
                        isDisplayed()));
        relativeLayout.perform(click());

        ViewInteraction relativeLayout2 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.content_protectores_detail),
                                0),
                        0),
                        isDisplayed()));
        relativeLayout2.check(matches(isDisplayed()));

        ViewInteraction floatingActionButton = onView(
                withId(R.id.agregarProtector));
        floatingActionButton.perform(scrollTo(), click());

        Thread.sleep(5000);
        ViewInteraction linearLayout = onView(
                allOf(withId(R.id.content_protectores_detail),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        linearLayout.check(matches(isDisplayed()));

        ViewInteraction linearLayout2 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.content_protectores_detail),
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.view.View.class),
                                        1)),
                        0),
                        isDisplayed()));
        linearLayout2.check(matches(isDisplayed()));


        ViewInteraction relativeLayout3 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.content_protectores_detail),
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.view.View.class),
                                        1)),
                        1),
                        isDisplayed()));
        relativeLayout3.check(matches(isDisplayed()));

        Thread.sleep(5000);
        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.textViewNombreProtectorEdit), withText("Clauuu"), isDisplayed()));
        appCompatEditText2.perform(click());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.textViewNombreProtectorEdit), withText("Clauuu"), isDisplayed()));
        appCompatEditText3.perform(replaceText("Claudia"), closeSoftKeyboard());


        ViewInteraction floatingActionButton2 = onView(
                allOf(withId(R.id.agregarProtectorEdit), isDisplayed()));
        floatingActionButton2.perform(click());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(android.R.id.button1), withText("Si")));
        appCompatButton2.perform(scrollTo(), click());
        Thread.sleep(5000);
        ViewInteraction relativeLayout4 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.content_protectores_detail),
                                0),
                        0),
                        isDisplayed()));
        relativeLayout4.check(matches(isDisplayed()));


        ViewInteraction appCompatImageButton3 = onView(
                allOf(withContentDescription("Navegar hacia arriba"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton3.perform(click());

        Thread.sleep(5000);
        ViewInteraction view3 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.lista_protectores),
                                0),
                        0),
                        isDisplayed()));
        view3.check(matches(isDisplayed()));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
