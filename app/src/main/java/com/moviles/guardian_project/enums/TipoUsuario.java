package com.moviles.guardian_project.enums;

/**
 * Created by Asus on 04/03/2017.
 */

public enum TipoUsuario {
    NORMAL,POLICIA,MECANICO, BOMBERO, PARAMEDICO, EJERCITO
}
