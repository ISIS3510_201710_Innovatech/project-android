package com.moviles.guardian_project.persistance;

/**
 * Created by Asus on 23/04/2017.
 */

public class TokenFirebase {
    public static final String TABLE_NAME="tokenfirebase";
    public static final String KEY_FAKE_NOMBRE="token";
    public static final String KEY_FAKE_ID="id";

    private long id;
    private String token;

    public TokenFirebase(long id, String token) {
        this.id = id;
        this.token = token;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
