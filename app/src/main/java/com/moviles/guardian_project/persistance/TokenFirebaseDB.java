package com.moviles.guardian_project.persistance;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 23/04/2017.
 */

public class TokenFirebaseDB extends SQLiteOpenHelper {
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TokenFirebase.TABLE_NAME + " (" +
                    TokenFirebase.KEY_FAKE_ID + " INTEGER PRIMARY KEY," +
                    TokenFirebase.KEY_FAKE_NOMBRE + " TEXT)" ;

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TokenFirebase.TABLE_NAME;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Tokenfirebase.db";
    public TokenFirebaseDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// This database is only a cache for online data, so its upgrade policy is
// to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void addFake( final TokenFirebase fake){
        SQLiteDatabase db=getWritableDatabase();
        db.beginTransaction();
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put(TokenFirebase.KEY_FAKE_NOMBRE,fake.getToken());
            final long plato_id=db.insertOrThrow(TokenFirebase.TABLE_NAME,null,contentValues);
            db.setTransactionSuccessful();
            fake.setId(plato_id);

        }catch (Throwable e){
            e.printStackTrace();

        }finally {
            db.endTransaction();
        }

    }

    public List<TokenFirebase> getAllFakes(){
        List<TokenFirebase>fakes=new ArrayList<>();
        String FAKE_QUERY=String.format("SELECT * FROM %s",TokenFirebase.TABLE_NAME);
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(FAKE_QUERY,null);
        try{
            if(cursor.moveToFirst()){
                do{
                    long fake_id=cursor.getLong(cursor.getColumnIndex(TokenFirebase.KEY_FAKE_ID));
                    String nombre = cursor.getString(cursor.getColumnIndex(TokenFirebase.KEY_FAKE_NOMBRE));

                    TokenFirebase fakeClass = new TokenFirebase(fake_id,nombre);
                    fakes.add(fakeClass);
                }while (cursor.moveToNext());
            }
        }catch (Throwable e){
            e.printStackTrace();

        }finally {
            if(cursor!=null && !cursor.isClosed()){
                cursor.close();
            }
        }
        return fakes;
    }
}
