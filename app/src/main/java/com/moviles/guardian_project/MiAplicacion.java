package com.moviles.guardian_project;

import android.app.Application;
import android.content.Context;

/**
 * Created by Johan on 9/03/2017.
 */

public class MiAplicacion extends Application {
    private String variableGlobal;

    public String getVariableGlobal() {
        return variableGlobal;
    }

    public void setVariableGlobal(String variableGlobal) {
        this.variableGlobal = variableGlobal;
    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }



}
