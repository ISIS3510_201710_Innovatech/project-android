package com.moviles.guardian_project.interfaces;

/**
 * Created by Asus on 24/03/2017.
 */

public interface BtnClickListener {
    public abstract void onBtnClick(int position);
}
