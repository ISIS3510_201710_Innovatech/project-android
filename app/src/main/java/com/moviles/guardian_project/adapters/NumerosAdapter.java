package com.moviles.guardian_project.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.moviles.guardian_project.R;
import com.moviles.guardian_project.entities.TelefonosEmergencia;
import com.moviles.guardian_project.interfaces.BtnClickListener;

import java.util.List;

/**
 * Created by Asus on 19/02/2017.
 */

public class NumerosAdapter extends BaseAdapter{
    private BtnClickListener mClickListener = null;
    private List<TelefonosEmergencia> numeros;
    private Context context;
    private static LayoutInflater inflater=null;

    public NumerosAdapter(Activity mainActivity, List<TelefonosEmergencia>protegidosList,BtnClickListener listener){
        this.numeros =protegidosList;
        this.context=mainActivity;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mClickListener =listener;
    }

    @Override
    public int getCount(){
        return numeros.size();
    }

    public Object getItem(int position){
        return position;
    }

    public long getItemId(int position){
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent){
        View view=convertView;
        TelefonosEmergencia emergencia= numeros.get(position);
        if(view==null){
            view=inflater.inflate(R.layout.numeros_item,null);

        }
        TextView nombreTextView=(TextView)view.findViewById(R.id.nombreTelefonoEmergencia);
        nombreTextView.setText(emergencia.getNombre());

        TextView numeroTextView=(TextView)view.findViewById(R.id.numeroTelefonoEmergencia);
        numeroTextView.setText(emergencia.getTelefonos());

        FloatingActionButton nuevoProtector2 = (FloatingActionButton)  view.findViewById(R.id.callEmergency);
        nuevoProtector2.setTag(position);
        nuevoProtector2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                if(mClickListener != null)
                    mClickListener.onBtnClick((Integer) view.getTag());

            }
        });

        return view;

    }
}
