package com.moviles.guardian_project.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.moviles.guardian_project.R;
import com.moviles.guardian_project.entities.EmergenciaAndroid;

import java.util.List;

/**
 * Created by Asus on 25/04/2017.
 */


public class ReportadasAdapter extends BaseAdapter {
    private List<EmergenciaAndroid> protegidos;
    private Context context;
    private static LayoutInflater inflater=null;

    public ReportadasAdapter(Activity mainActivity, List<EmergenciaAndroid>protegidosList){
        this.protegidos =protegidosList;
        this.context=mainActivity;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount(){
        return protegidos.size();
    }

    public Object getItem(int position){
        return position;
    }

    public long getItemId(int position){
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent){
        View view=convertView;
        EmergenciaAndroid emergencia= protegidos.get(position);
        if(view==null){
            view=inflater.inflate(R.layout.emergencias_reportadas_item,null);

        }
        TextView nombreTextView=(TextView)view.findViewById(R.id.fechaReportadas);
        nombreTextView.setText(emergencia.getFecha());
        TextView mensaje=(TextView)view.findViewById(R.id.mensajeReportadas);
        mensaje.setText(emergencia.getMensaje());
        TextView motivoTextView=(TextView)view.findViewById(R.id.tipoReportada);
        motivoTextView.setText(emergencia.getTipo());
        return view;

    }

}

