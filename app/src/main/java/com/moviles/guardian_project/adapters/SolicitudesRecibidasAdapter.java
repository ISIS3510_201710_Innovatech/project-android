package com.moviles.guardian_project.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.moviles.guardian_project.R;
import com.moviles.guardian_project.entities.Solicitud;

import java.util.List;

/**
 * Created by Asus on 30/03/2017.
 */

public class SolicitudesRecibidasAdapter extends BaseAdapter {
    private List<Solicitud> protegidos;
    private Context context;
    private static LayoutInflater inflater=null;

    public SolicitudesRecibidasAdapter(Activity mainActivity, List<Solicitud>protegidosList){
        this.protegidos =protegidosList;
        this.context=mainActivity;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount(){
        return protegidos.size();
    }

    public Object getItem(int position){
        return position;
    }

    public long getItemId(int position){
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent){
        View view=convertView;
        Solicitud emergencia= protegidos.get(position);
        if(view==null){
            view=inflater.inflate(R.layout.solicitudes_item,null);

        }
        TextView nombreTextView=(TextView)view.findViewById(R.id.fechaReportadas);
        nombreTextView.setText(emergencia.getParaNombre());
        TextView motivoTextView=(TextView)view.findViewById(R.id.tipoReportada);
        motivoTextView.setText(emergencia.getTipo());
        TextView correoTextView=(TextView)view.findViewById(R.id.correoAgregar);
        correoTextView.setText(emergencia.getParaCorreo());
        return view;

    }
}
