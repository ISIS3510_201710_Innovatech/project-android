package com.moviles.guardian_project.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.moviles.guardian_project.R;
import com.moviles.guardian_project.entities.Solicitud;
import com.moviles.guardian_project.interfaces.BtnClickListener;

import java.util.List;

/**
 * Created by Asus on 30/03/2017.
 */

public class MiasSolicitudesAdapter extends BaseAdapter {
    private BtnClickListener mClickListenerSi = null;
    private BtnClickListener mClickListenerNo = null;
    private List<Solicitud> protegidos;
    private Context context;
    private static LayoutInflater inflater=null;

    public MiasSolicitudesAdapter(Activity mainActivity, List<Solicitud>protegidosList,BtnClickListener listenerSi,BtnClickListener listenerNo){
        this.protegidos =protegidosList;
        this.context=mainActivity;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mClickListenerSi =listenerSi;
        mClickListenerNo =listenerNo;
    }

    @Override
    public int getCount(){
        return protegidos.size();
    }

    public Object getItem(int position){
        return position;
    }

    public long getItemId(int position){
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent){
        View view=convertView;
        Solicitud emergencia= protegidos.get(position);
        if(view==null){
            view=inflater.inflate(R.layout.mias_solicitudes_item,null);
        }
        TextView nombreTextView=(TextView)view.findViewById(R.id.fechaReportadas);
        nombreTextView.setText(emergencia.getDeNombre());
        TextView motivoTextView=(TextView)view.findViewById(R.id.motivo_mias);
        motivoTextView.setText(emergencia.getTipo());
        TextView correoTextView=(TextView)view.findViewById(R.id.mensajeReportadas);
        correoTextView.setText(emergencia.getDeCorreo());

        FloatingActionButton nuevoProtector2 = (FloatingActionButton)  view.findViewById(R.id.aceptarSolicitud_mias);
        nuevoProtector2.setTag(position);
        nuevoProtector2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                if(mClickListenerSi != null)
                    mClickListenerSi.onBtnClick((Integer) view.getTag());

            }
        });

        FloatingActionButton nuevoProtector3 = (FloatingActionButton)  view.findViewById(R.id.rechazarSolicitud_mias);
        nuevoProtector3.setTag(position);
        nuevoProtector3.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                if(mClickListenerNo != null)
                    mClickListenerNo.onBtnClick((Integer) view.getTag());
            }
        });

        return view;

    }


}
