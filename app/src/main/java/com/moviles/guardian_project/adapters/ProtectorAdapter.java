package com.moviles.guardian_project.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.moviles.guardian_project.R;
import com.moviles.guardian_project.entities.UsuarioProtegido;

import java.util.List;

/**
 * Created by Asus on 31/03/2017.
 */

public class ProtectorAdapter extends BaseAdapter {
    private List<UsuarioProtegido> protegidos;
    private Context context;
    private static LayoutInflater inflater=null;

    public ProtectorAdapter(Activity mainActivity, List<UsuarioProtegido>protegidosList){
        this.protegidos =protegidosList;
        this.context=mainActivity;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount(){
        return protegidos.size();
    }

    public Object getItem(int position){
        return position;
    }

    public long getItemId(int position){
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent){
        View view=convertView;
        UsuarioProtegido emergencia= protegidos.get(position);
        if(view==null){
            view=inflater.inflate(R.layout.protector_item,null);

        }
        TextView nombreTextView=(TextView)view.findViewById(R.id.nombreProtectorItem);
        nombreTextView.setText(emergencia.getNombre());
        return view;

    }
}
