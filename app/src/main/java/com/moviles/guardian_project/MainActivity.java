package com.moviles.guardian_project;

import android.Manifest;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.newrelic.agent.android.NewRelic;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.moviles.guardian_project.activities.login.Login2;
import com.moviles.guardian_project.entities.GPSReceiver;
import com.moviles.guardian_project.entities.UserAnroid;
import com.moviles.guardian_project.fragmentos.GmapFragment;
import com.moviles.guardian_project.fragmentos.ListNumerosFragment;
import com.moviles.guardian_project.fragmentos.ListProtectoresFragment;
import com.moviles.guardian_project.fragmentos.ListProtegidosFragment;
import com.moviles.guardian_project.fragmentos.ListReportadasFragment;
import com.moviles.guardian_project.fragmentos.SolicitudesEnviadasFragment;
import com.moviles.guardian_project.fragmentos.SolicitudesRecibidasFragment;
import com.moviles.guardian_project.persistance.TokenFirebase;
import com.moviles.guardian_project.persistance.TokenFirebaseDB;

import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "Actividad Main";
    public static int vista=0;
    public static String ID_DE_USUARIO_ACTUAL=null;
    public static String CORREO_DE_USUARIO_ACTUAL=null;
    private FirebaseDatabase database;
    private DatabaseReference myRef;

    public final static String GPS_FILTER ="GPSFilter";
    public  static boolean tokenDesactualizado=true;
    public GPSReceiver receiver;
    public static UserAnroid yo=null;


    public TokenFirebaseDB databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        // ********* SERVICE *******
      if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION )!= PackageManager.PERMISSION_GRANTED
                &&   ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION )!= PackageManager.PERMISSION_GRANTED) {
           ActivityCompat.requestPermissions(this,
                   new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},1);

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        if (getIntent() != null &&getIntent().getExtras() != null && getIntent().getExtras().getString("userId") != null && getIntent().getExtras().getString("correo")!=null) {
            ID_DE_USUARIO_ACTUAL = getIntent().getExtras().getString("userId");
            CORREO_DE_USUARIO_ACTUAL = getIntent().getExtras().getString("correo");
            Log.e(TAG, "idDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD "+ID_DE_USUARIO_ACTUAL);

        }



        if(this !=null) {
            try {
                database = FirebaseDatabase.getInstance();
                database = FirebaseDatabase.getInstance();
                myRef = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL);


                myRef.addListenerForSingleValueEvent(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        Log.e("333333333333",dataSnapshot.toString());
                         yo = dataSnapshot.getValue(UserAnroid.class);



                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        Log.w(TAG, "Failed to read value.", error.toException());
                    }


                });
            } catch (Throwable e) {
                AlertDialog alertDialog = createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo", "Error");
                alertDialog.show();
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
            }
        }

        if(tokenDesactualizado){
            try {
                DatabaseReference myRef;
                databaseHelper = new TokenFirebaseDB(this);
                database = FirebaseDatabase.getInstance();
                myRef = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL);
                List<TokenFirebase> fakeClasses = databaseHelper.getAllFakes();
                myRef.child("token").setValue(fakeClasses.get(fakeClasses.size() - 1).getToken());
                tokenDesactualizado = false;
            }catch (Throwable e){
                e.printStackTrace();
            }
        }

        FragmentManager fm=getFragmentManager();


        if (vista == 0) {
            fm.beginTransaction().replace(R.id.content_frame,new GmapFragment()).commit();
            getSupportActionBar().setTitle("Guardian");
        }else if (vista == 1) {
            fm.beginTransaction().replace(R.id.content_frame,new ListProtegidosFragment()).commit();
            getSupportActionBar().setTitle("Protegidos");
        } else if (vista == 2) {
            fm.beginTransaction().replace(R.id.content_frame,new ListProtectoresFragment()).commit();
            getSupportActionBar().setTitle("Protectores");
        } else if (vista == 3) {
            fm.beginTransaction().replace(R.id.content_frame,new ListNumerosFragment()).commit();
            getSupportActionBar().setTitle("Numeros");
        } else if (vista == 4) {
            fm.beginTransaction().replace(R.id.content_frame,new ListReportadasFragment()).commit();
            getSupportActionBar().setTitle("Historial");
        }else if(vista == 5){
            fm.beginTransaction().replace(R.id.content_frame,new SolicitudesRecibidasFragment()).commit();
            getSupportActionBar().setTitle("Recibidas");
        }else if(vista == 6){
            fm.beginTransaction().replace(R.id.content_frame,new SolicitudesEnviadasFragment()).commit();
            getSupportActionBar().setTitle("Enviadas");
        }
        else{
            fm.beginTransaction().replace(R.id.content_frame,new GmapFragment()).commit();
            getSupportActionBar().setTitle("Guardian");
        }

       // NewRelic.withApplicationToken(
       //         "AAd78d5ba42a98c4f2e29e31e490ecd63cfc19fbd1"
       // ).start(this.getApplication());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            AlertDialog alertDialog=createAlertDialog();
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
            return true;
        }else if (id == R.id.action_logout) {
            try {
                FirebaseAuth.getInstance().signOut();
                Intent i = new Intent(MainActivity.this, Login2.class);
                Log.e(TAG, "LOGOUT **");
                startActivity(i);
                finish();
            }catch(Exception e){
                Log.e(TAG, "No se pudo hacer LOGOUT **");
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public AlertDialog createAlertDialog(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage("Esta es una aplicación que le permitirá enviar mensajes de alerta cuando se encuentre en una situación de emeregencia.")
                .setTitle("Información")
        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return builder.create();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        FragmentManager fm=getFragmentManager();
        int id = item.getItemId();
        if (id == R.id.nav_inicio) {
            fm.beginTransaction().replace(R.id.content_frame,new GmapFragment()).commit();
            getSupportActionBar().setTitle("Guardian");
            vista=0;
        } else if (id == R.id.nav_protegidos) {
            fm.beginTransaction().replace(R.id.content_frame,new ListProtegidosFragment()).commit();
            getSupportActionBar().setTitle("Protegidos");
            vista=1;
        } else if (id == R.id.nav_protectores) {
            fm.beginTransaction().replace(R.id.content_frame,new ListProtectoresFragment()).commit();
            getSupportActionBar().setTitle("Protectores");
            vista=2;
        } else if (id == R.id.nav_numeros) {
            fm.beginTransaction().replace(R.id.content_frame,new ListNumerosFragment()).commit();
            getSupportActionBar().setTitle("Numeros");
            vista=3;
        } else if (id == R.id.nav_reportadas) {
            fm.beginTransaction().replace(R.id.content_frame,new ListReportadasFragment()).commit();
            getSupportActionBar().setTitle("Historial");
            vista=4;
        }else if(id == R.id.nav_mias_solicitudes){
            fm.beginTransaction().replace(R.id.content_frame,new SolicitudesRecibidasFragment()).commit();
            getSupportActionBar().setTitle("Recibidas");
            vista=5;
        }else if(id == R.id.nav_solicitudes_enviadas){
            fm.beginTransaction().replace(R.id.content_frame,new SolicitudesEnviadasFragment()).commit();
            getSupportActionBar().setTitle("Enviadas");
            vista=6;
        }
        else{
            fm.beginTransaction().replace(R.id.content_frame,new GmapFragment()).commit();
            getSupportActionBar().setTitle("Guardian");
            vista=0;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public AlertDialog createAlertDialog(String msg,String title){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);

        builder.setMessage(msg)
                .setTitle(title)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }

}
