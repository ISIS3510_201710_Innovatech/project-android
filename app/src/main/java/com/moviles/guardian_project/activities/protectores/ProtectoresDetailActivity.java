package com.moviles.guardian_project.activities.protectores;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.moviles.guardian_project.MainActivity;
import com.moviles.guardian_project.R;
import com.moviles.guardian_project.activities.protectores.ProtectoresEditActivity;
import com.moviles.guardian_project.entities.Protegido;
import com.moviles.guardian_project.entities.Usuario;
import com.moviles.guardian_project.entities.UsuarioProtegido;


import java.util.ArrayList;
import java.util.List;

public class ProtectoresDetailActivity extends AppCompatActivity {

    public final static int EDITAR=11;
    private static final String TAG="editar";

    private String nombre="";
    private String id="";
    private String correo="";
    private String telefono="";
    private String direccion="";

    private TextView correoTV, nombreTV, direccionTV,telefonoTV;

    private String idUsuario;

    // Write a message to the database
   // private FirebaseDatabase database;
    private FirebaseDatabase database;
    private DatabaseReference myRef;


    private String protectorName="nn";

    private ProtectoresDetailActivity este;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_protectores_detail);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            este = this;

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);


            //FIREBASE

            idUsuario = MainActivity.ID_DE_USUARIO_ACTUAL;


            database = FirebaseDatabase.getInstance();
            myRef = database.getInstance().getReference("usuariosAndroid").child(idUsuario).child("protectores");


            correoTV = (TextView) findViewById(R.id.textViewCorreoProtector);
            nombreTV = (TextView) findViewById(R.id.textViewNombreProtector);
            direccionTV = (TextView) findViewById(R.id.textViewDireccionProtector);
            telefonoTV = (TextView) findViewById(R.id.textViewTelefonoProtector);


            if (getIntent().getExtras().getString("id") != null)
                id = getIntent().getExtras().getString("id");

            if (id != null && id != "") {
                getUser(id);
            }


            FloatingActionButton agregarProtector = (FloatingActionButton) findViewById(R.id.agregarProtector);
            agregarProtector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Intent i = new Intent(este, ProtectoresEditActivity.class);
                    i.putExtra("name", protectorName);
                    i.putExtra("correo", correo);
                    i.putExtra("telefono", telefono);
                    i.putExtra("direccion", direccion);
                    i.putExtra("id", id);
                    i.putExtra("idUsuario", MainActivity.ID_DE_USUARIO_ACTUAL);
                    startActivity(i);

                }
            });


            FloatingActionButton eliminarProtector = (FloatingActionButton) findViewById(R.id.eliminarProtector);
            eliminarProtector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    if (id != null) deleteUser(id);

                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(este);
                    builder.setMessage("¿Está seguro que desea eliminar a este usuario?").setPositiveButton("Si", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener);
                    AlertDialog alert = builder.create();
                    alert.show();
                    alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                }
            });
        }catch (Throwable e){
            try{
                AlertDialog alertDialog=createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo","Error");
                alertDialog.show();
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
            }catch (Throwable ee){

            }
        }

    }


    public AlertDialog createAlertDialog(String msg,String title){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);

        builder.setMessage(msg)
                .setTitle(title)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }

    private void getUser(String idN){
        if(this!=null) {
            try {

                myRef.child(idN).addListenerForSingleValueEvent(new ValueEventListener() {


                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {


                        UsuarioProtegido value = dataSnapshot.getValue(UsuarioProtegido.class);
                        Log.e(TAG, "aAAAA " + dataSnapshot);

                        if (value != null) {

                            if (value.getNombre() != null) protectorName = value.getNombre();
                            if (value.getCorreo() != null) correo = value.getCorreo();
                            if (value.getTelefono() != null) telefono = value.getTelefono();
                            if (value.getDireccion() != null) direccion = value.getDireccion();
                            if (getIntent().getExtras().getString("id") != null)
                                id = getIntent().getExtras().getString("id");


                            getSupportActionBar().setTitle(protectorName);
                            correoTV.setText(correo);
                            nombreTV.setText(protectorName);
                            direccionTV.setText(direccion);
                            telefonoTV.setText(telefono);

                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        Log.e(TAG, "Failed to read value.", error.toException());
                    }

                });
            }catch (Throwable ee){
                try{
                    AlertDialog alertDialog = createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo", "Error");
                    alertDialog.show();
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                }catch (Throwable e){

                }
            }
        }
    }



    private void deleteUser(String idN) {
        myRef.child(idN).setValue(null);
        AlertDialog alertDialog=createAlertDialog("El contacto se borró satisfactoriamente","¡Exitoso!");
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);

    }



    public void onResume(){
        try {
            super.onResume();
            if (id != null) getUser(id);
        }catch (Throwable e){
            AlertDialog alertDialog=createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo","Error");
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        }
    }

}
