package com.moviles.guardian_project.activities.emergencias.reportadas;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.moviles.guardian_project.MainActivity;
import com.moviles.guardian_project.R;
import com.moviles.guardian_project.adapters.AtendidasAdapter;
import com.moviles.guardian_project.adapters.ReportadasAdapter;
import com.moviles.guardian_project.entities.EmergenciaAndroid;
import com.moviles.guardian_project.entities.EmergenciaAtendida;

import java.util.ArrayList;
import java.util.List;

public class EmergenciasReportadasActivity extends AppCompatActivity {

    List<EmergenciaAndroid> emergencias;
    ListView listView;
    Activity actividad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergencias_reportadas);
        listView = (ListView) findViewById(R.id.lista_reportadas);
        actividad=this;

        FirebaseDatabase database=FirebaseDatabase.getInstance();
        database.getInstance().getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("emergenciasReportadas").limitToLast(15).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(this!=null) {
                    emergencias=new ArrayList<EmergenciaAndroid>();
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        EmergenciaAndroid e=postSnapshot.getValue(EmergenciaAndroid.class);
                        emergencias.add(e);
                        ReportadasAdapter items = new ReportadasAdapter(actividad,emergencias);

                        listView.setAdapter(items);
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
}
