package com.moviles.guardian_project.activities.login;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.moviles.guardian_project.MainActivity;
import com.moviles.guardian_project.R;
import com.moviles.guardian_project.activities.registro.RegistroActivity;
import com.moviles.guardian_project.entities.Protegido;
import com.moviles.guardian_project.entities.UserAnroid;

public class Login2 extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;


    private String emailUsuario;

    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;
    private static final String TAG = "SignInActivity";


    private CallbackManager mCallbackManager;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private LoginButton loginButton;

    private TextView texto;
    private  static boolean b=false;

    private boolean registrado;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        texto= (TextView)findViewById(R.id.texto2);

        registrado=false;

        if(!b) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            b=true;
        }


        //***********LOGIN CON CORREO*****************

        Button loginCorreo = (Button) findViewById(R.id.login_correo2);


        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);

        loginCorreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mEmailView.getText()!=null && mPasswordView.getText()!= null && mEmailView.getText().toString()!=null
                        && mPasswordView.getText().toString()!= null && !mPasswordView.getText().toString().equals("")
                        && !mEmailView.getText().toString().equals("")) {
                    signIn(mEmailView.getText().toString(), mPasswordView.getText().toString());

                }
                else{

                    AlertDialog alertDialog = createAlertDialog("Ingrese un usuario y contraseña válido.", "¡Error al iniciar sesión!");
                    alertDialog.show();
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                    //texto.setText("Ingrese un usuario y una contraseña");
                }
            }
        });


        //**********REGISTRO*****
        final Button registro =(Button) findViewById(R.id.registro2);

        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Login2.this, RegistroActivity.class);
                startActivity(i);
            }
        });


        //******************AUTENTICACION FIREBASE ***************

     mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        // [START auth_state_listener]
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    texto= (TextView)findViewById(R.id.texto2);
                    texto.setText("Se encuentra logueado");
                     Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());

                    Log.e(TAG, "Logueando:   " + user.getUid());

                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference myRef1=database.getReference("usuariosAndroid");
                    myRef1.addValueEventListener(new ValueEventListener() {
                          @Override
                          public void onDataChange(DataSnapshot dataSnapshot) {
                              for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                  Log.e(TAG, "BUSCANDO:   " + postSnapshot.toString());
                                  Protegido value = postSnapshot.getValue(Protegido.class);
                                  Log.e(TAG,"COMPARANDO "+value.getId() +"     "+user.getUid());

                                  if (value.getId().equals(user.getUid())) {
                                      registrado = true;
                                  }
                              }

                              if(registrado==true)
                                  iniciarAplicacion(user.getUid(),user.getEmail());
                              else{
                                  Intent i = new Intent(Login2.this, RegistroActivity.class);
                                  i.putExtra("userId",user.getUid()+"");
                                  i.putExtra("correo",user.getEmail());

                                  Log.e(TAG, "id "+user.getUid());
                                  Log.e(TAG, "correo "+user.getEmail());
                                  startActivity(i);
                              }
                          }

                          @Override
                          public void onCancelled(DatabaseError error) {
                              // Failed to read value
                              Log.w(TAG, "Failed to read value.", error.toException());
                          }
                      });




                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // [START_EXCLUDE]

                // [END_EXCLUDE]
            }
        };

        //**********GOOGLE*******************************

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button2);
        signInButton.setSize(SignInButton.SIZE_STANDARD);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                texto= (TextView)findViewById(R.id.texto2);
                texto.setText("Conectandose con Google");
                signIn();

            }
        });


        // *****************FACEBOOK ***************
        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button_facebook);
        loginButton.setReadPermissions("email");




        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e(TAG, "facebook:onSuccess:" + loginResult);


               // Intent i = new Intent(Login2.this, MainActivity.class);
               // startActivity(i);
                //handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.e(TAG, "facebook:onCancel");
               // Intent i = new Intent(Login2.this, MainActivity.class);
               // startActivity(i);
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(TAG, "facebook:onError", error);
              //  Intent i = new Intent(Login2.this, MainActivity.class);
              //  startActivity(i);
                // ...
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
       mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }



    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);



        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());



                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail:failed", task.getException());
                            Toast.makeText(Login2.this, R.string.auth_failed,
                                    Toast.LENGTH_SHORT).show();

                            AlertDialog alertDialog = createAlertDialog("Ingrese un usuario y contraseña válido.", "¡Falló la autenticación!");
                            alertDialog.show();
                            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);

                            texto.setText("Ingrese su usuario y contraseña correctamente" );
                            return;
                        }

                        // [START_EXCLUDE]
                        if (!task.isSuccessful()) {

                            AlertDialog alertDialog = createAlertDialog("Ingrese un usuario y contraseña válido.", "¡Falló la autenticación!");
                            alertDialog.show();
                            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                            texto.setText(R.string.auth_failed);
                            texto.setText("Ingrese su usuario y contraseña correctamente" );
                            return;
                        }

/*
                        Intent i = new Intent(Login2.this, MainActivity.class);
                        startActivity(i);*/

                       // iniciarAplicacion();


                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with_email]
    }



    public boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    public boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */



/*
    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.sign_in_button) {

            signIn();
        }
        else if(i==R.id.login_correo){
            TextView texto= (TextView)findViewById(R.id.texto);
            texto.setText("BOTON");
        }
            /*AutoCompleteTextView login=(AutoCompleteTextView)findViewById(R.id.email);
            EditText password=(EditText)findViewById(R.id.password);
            Log.d("LOGIN","CONECTANDOSE AAAAAAAAAAAAAAAAAAAAAAAAAA "+login.getText().toString());
            signIn(login.getText().toString(), password.getText().toString());

    }*/

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

        mCallbackManager.onActivityResult(requestCode, resultCode, data);

    }





    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            firebaseAuthWithGoogle(acct);

            texto=(TextView) findViewById(R.id.texto2);
            texto.setText(acct.getDisplayName());

            //iniciarAplicacion(acct.getId(),acct.getEmail());

        } else {
            // Signed out, show unauthenticated UI.

        }
    }


    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.e(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.e(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                         texto.setText("Se conecto con google");

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(Login2.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                            AlertDialog alertDialog = createAlertDialog("Ingrese con una cuenta válida de Google o Firebase.", "¡Falló la autenticación!");
                            alertDialog.show();
                            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);

                        }
                        // ...
                    }
                });
    }





    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AlertDialog alertDialog = createAlertDialog("Intente conectarse de nuevo.", "¡Falló la conexión!");
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        Log.e("Connection failed","Conexion de google fallo");
    }


    public void iniciarAplicacion( String id, String correo){
        Intent i = new Intent(Login2.this, MainActivity.class);
        i.putExtra("userId",id+"");
        i.putExtra("correo",correo);

        Log.e(TAG, "id "+id);
        Log.e(TAG, "correo "+correo);
        startActivity(i);
        finish();

    }

    public void iniciarAplicacion( ){
        Intent i = new Intent(Login2.this, MainActivity.class);
        i.putExtra("userId","@-@");
        i.putExtra("correo",emailUsuario);
        Log.e(TAG, "Email "+emailUsuario);
        startActivity(i);
        finish();

    }

    public AlertDialog createAlertDialog(String msg, String title){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);

        builder.setMessage(msg)
                .setTitle(title)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
