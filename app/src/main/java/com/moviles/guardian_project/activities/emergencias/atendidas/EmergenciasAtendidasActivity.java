package com.moviles.guardian_project.activities.emergencias.atendidas;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.moviles.guardian_project.MainActivity;
import com.moviles.guardian_project.R;
import com.moviles.guardian_project.adapters.AtendidasAdapter;
import com.moviles.guardian_project.adapters.ProtegidoAdapter;
import com.moviles.guardian_project.entities.EmergenciaAtendida;

import java.util.ArrayList;
import java.util.List;

public class EmergenciasAtendidasActivity extends AppCompatActivity {

          List<EmergenciaAtendida> emergencias;
                        ListView listView;
                        Activity actividad;
                        @Override
                        protected void onCreate(Bundle savedInstanceState) {
                            super.onCreate(savedInstanceState);
                            setContentView(R.layout.activity_emergencias_atendidas);
                            listView = (ListView) findViewById(R.id.lista_atendidas);
                            actividad=this;

                            FirebaseDatabase database=FirebaseDatabase.getInstance();
                            database.getInstance().getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("emergenciasAtendidas").limitToLast(15).addListenerForSingleValueEvent(new ValueEventListener() {

                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(this!=null) {
                                        emergencias=new ArrayList<EmergenciaAtendida>();
                                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                            EmergenciaAtendida e=postSnapshot.getValue(EmergenciaAtendida.class);
                                            emergencias.add(e);
                        AtendidasAdapter items = new AtendidasAdapter(actividad,emergencias);
                        //ArrayAdapter<String> items1=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,nombres);


                        listView.setAdapter(items);
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
}
