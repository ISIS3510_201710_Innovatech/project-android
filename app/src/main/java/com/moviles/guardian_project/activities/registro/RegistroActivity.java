package com.moviles.guardian_project.activities.registro;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.moviles.guardian_project.MainActivity;
import com.moviles.guardian_project.R;
import com.moviles.guardian_project.activities.login.Login2;
import com.moviles.guardian_project.entities.UserAnroid;
import com.moviles.guardian_project.entities.Usuario;

import java.util.Calendar;
import java.util.Map;

public class RegistroActivity extends AppCompatActivity {

    private EditText inputEmail, inputPassword;
    private EditText nombre,cedula,direccion,genero,telefono,fechaNacimiento ;

    private FloatingActionButton btnSignUp, btnRetroceder;
   // private ProgressBar progressBar;
    private FirebaseAuth auth;
    FirebaseDatabase database;

    private DatePickerFragment datePickerFragment;

    public static String ID_DE_USUARIO_ACTUAL=null;
    public static String CORREO_DE_USUARIO_ACTUAL=null;
    public static String TAG="Registro";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);



        if (getIntent() != null &&getIntent().getExtras() != null && getIntent().getExtras().getString("userId") != null && getIntent().getExtras().getString("correo")!=null) {
            ID_DE_USUARIO_ACTUAL = getIntent().getExtras().getString("userId");
            CORREO_DE_USUARIO_ACTUAL = getIntent().getExtras().getString("correo");
            Log.e(TAG, "ID  NUEVO "+ID_DE_USUARIO_ACTUAL+ " CORREO "+CORREO_DE_USUARIO_ACTUAL);

        }

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        btnSignUp = (FloatingActionButton) findViewById(R.id.enviarRegistro);
        btnRetroceder = (FloatingActionButton) findViewById(R.id.noRegistro);

        inputEmail = (EditText) findViewById(R.id.newCorreo);
        inputPassword = (EditText) findViewById(R.id.newContrasenia);

        nombre = (EditText) findViewById(R.id.newNombre);
        cedula = (EditText) findViewById(R.id.newCedula);
        direccion= (EditText) findViewById(R.id.newDireccion);
        genero = (EditText) findViewById(R.id.newGenero);
        telefono = (EditText) findViewById(R.id.newTelefono);
        fechaNacimiento =  (EditText) findViewById(R.id.newFecha);

        fechaNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerFragment= new DatePickerFragment();
                datePickerFragment.fecha=fechaNacimiento;
                datePickerFragment.show(getSupportFragmentManager(), "datePicker");
            }
        } );

        if(CORREO_DE_USUARIO_ACTUAL!=null) {
            inputEmail.setText(CORREO_DE_USUARIO_ACTUAL);
            inputPassword.setText("noNecesario");
        }
        //sexo =(EditText) findViewById(R.id.new);;

        //progressBar = (ProgressBar) findViewById(R.id.progressBar);

        btnRetroceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistroActivity.this, Login2.class));
            }
        });


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (connectedToWiFi3())
                {

                    String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }


                    if (!email.contains("@")) {
                        Toast.makeText(getApplicationContext(), "¡Introduzca un correo valido!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (TextUtils.isEmpty(nombre.getText().toString()) || TextUtils.isEmpty(cedula.getText().toString()) || TextUtils.isEmpty(direccion.getText().toString())
                        || TextUtils.isEmpty(genero.getText().toString()) || TextUtils.isEmpty(telefono.getText().toString()) || TextUtils.isEmpty(fechaNacimiento.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (password.length() < 6) {
                    Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                    return;
                }

                //progressBar.setVisibility(View.VISIBLE);
                //create user

                if (CORREO_DE_USUARIO_ACTUAL == null) {

                    auth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(RegistroActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Toast.makeText(RegistroActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                                    // progressBar.setVisibility(View.GONE);
                                    // If sign in fails, display a message to the user. If sign in succeeds
                                    // the auth state listener will be notified and logic to handle the
                                    // signed in user can be handled in the listener.
                                    if (!task.isSuccessful()) {
                                        Toast.makeText(RegistroActivity.this, "Authentication failed." + task.getException(),
                                                Toast.LENGTH_SHORT).show();
                                    } else {

                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                        if (user != null) {
                                            // User is signed in

                                            DatabaseReference myRef = database.getReference("usuariosAndroid");
                                            UserAnroid nuevo = new UserAnroid();

                                            nuevo.setCedula(cedula.getText().toString());
                                            nuevo.setCorreo(inputEmail.getText().toString().trim());
                                            nuevo.setDireccion(direccion.getText().toString());
                                            nuevo.setEstado("Normal");
                                            nuevo.setNombre(nombre.getText().toString());
                                            nuevo.setFechaNacimiento(fechaNacimiento.getText().toString());
                                            nuevo.setGenero(genero.getText().toString());
                                            nuevo.setTelefono(telefono.getText().toString());
                                            nuevo.setTipoUsuario("Normal");

                                            // String key = myRef.push().getKey();

                                            String key = user.getUid();
                                            Log.e("REGISTRO", "Registrando " + nuevo.getCedula() + " " + key);

                                            nuevo.setId(key);
                                            myRef.child(key).setValue(nuevo);

                                            Intent i = new Intent(RegistroActivity.this, Login2.class);
                                            startActivity(i);
                                            finish();

                                        } else {
                                            Toast.makeText(RegistroActivity.this, "Authentication failed." + task.getException(),
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                   /* i.putExtra("userId",id+"");
                                    i.putExtra("correo",correo);*/
                                        // startActivity(i);
                                        //finish();
                                    }
                                }
                            });

                } else {

                    Log.e(TAG, "Creando nuevo usuario Google " + CORREO_DE_USUARIO_ACTUAL + " ID: " + ID_DE_USUARIO_ACTUAL);

                    DatabaseReference myRef = database.getReference("usuariosAndroid");
                    UserAnroid nuevo = new UserAnroid();

                    nuevo.setCedula(cedula.getText().toString());
                    nuevo.setCorreo(inputEmail.getText().toString().trim());
                    nuevo.setDireccion(direccion.getText().toString());
                    nuevo.setEstado("Normal");
                    nuevo.setNombre(nombre.getText().toString());
                    // nuevo.setFechaNacimiento(fechaNacimiento.getText().toString());
                    nuevo.setGenero(genero.getText().toString());
                    nuevo.setTelefono(telefono.getText().toString());
                    nuevo.setTipoUsuario("Normal");

                    // String key = myRef.push().getKey();

                    if (ID_DE_USUARIO_ACTUAL != null) {
                        String key = ID_DE_USUARIO_ACTUAL;
                        Log.e("REGISTRO", "Registrando " + nuevo.getCedula() + " " + key);

                        nuevo.setId(key);
                        myRef.child(key).setValue(nuevo);

                        Intent i = new Intent(RegistroActivity.this, Login2.class);
                        startActivity(i);
                        finish();
                    }

                }

            }
                else{
                    AlertDialog alertDialog = createAlertDialog("No se encuentra conectado a Internet ", "¡Error!");
                    alertDialog.show();
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);

                }
        }
        });



    }



    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        public int anio,mes,dia;
        public EditText fecha;



        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            dia=day;
            mes=month;
            anio=year;
            fecha.setText(dia +"/"+mes+"/"+anio);

        }

        public String setFecha(){

            return dia +"/"+mes+"/"+anio;
        }
    }

    private boolean connectedToWiFi3() {
        ConnectivityManager connManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    public AlertDialog createAlertDialog(String msg, String title){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);

        builder.setMessage(msg)
                .setTitle(title)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }
}
