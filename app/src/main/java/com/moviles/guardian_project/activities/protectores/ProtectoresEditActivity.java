package com.moviles.guardian_project.activities.protectores;


import android.content.Context;
import android.content.DialogInterface;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import android.widget.EditText;
import android.widget.LinearLayout;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.moviles.guardian_project.MainActivity;
import com.moviles.guardian_project.R;

import com.moviles.guardian_project.entities.Protegido;
import com.moviles.guardian_project.entities.Solicitud;
import com.moviles.guardian_project.entities.UsuarioProtegido;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ProtectoresEditActivity extends AppCompatActivity {

    public final static int EDITAR=11;
    private static final String TAG="editar";


    private UsuarioProtegido user;
    private String idUserAgregar;
    private String id;
    private String correo;
    private String telefono;
    private String direccion;
    private String idUsuario;
    private Solicitud solicitud;
    private String correoUsuarioAgregar;
    private List<UsuarioProtegido> listUsuariosProtegidos;
    private Map<String, Object> childUpdates;
    private boolean v;
    private boolean w;

    EditText idTV;

    EditText nombreTV;


    LinearLayout protegidoNuevo;


    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private DatabaseReference ref;
    private DatabaseReference protectoresRef;
    private Protegido actual;

    private DatabaseReference mDatabase;




    private String protegidoName;
    private ProtectoresEditActivity este;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_protectores_edit);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            listUsuariosProtegidos=new ArrayList<UsuarioProtegido>();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);


            if (getIntent().getExtras().getString("idUsuario") != null)
                idUsuario = getIntent().getExtras().getString("idUsuario");

            este=this;
            correoUsuarioAgregar="";
            database = FirebaseDatabase.getInstance();
            myRef = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("protectores");
            ref=database.getReference("solicitudes");

            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(this!=null) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            UsuarioProtegido value = postSnapshot.getValue(UsuarioProtegido.class);
                            listUsuariosProtegidos.add(value);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            idTV = (EditText) findViewById(R.id.usuarioProtectorEdit);
            nombreTV = (EditText) findViewById(R.id.textViewNombreProtectorEdit);

            if (getIntent() != null && getIntent().getExtras().getString("id") != null) {

                protegidoNuevo = (LinearLayout) findViewById(R.id.protectorNuevo);
                protegidoNuevo.setVisibility(View.INVISIBLE);
                id = getIntent().getExtras().getString("id");
                if (getIntent().getExtras().getString("name") != null)
                    protegidoName = getIntent().getExtras().getString("name");
                if (getIntent().getExtras().getString("correo") != null)
                    correo = getIntent().getExtras().getString("correo");
                if (getIntent().getExtras().getString("telefono") != null)
                    telefono = getIntent().getExtras().getString("telefono");
                if (getIntent().getExtras().getString("direccion") != null)
                    direccion = getIntent().getExtras().getString("direccion");

            } else {
                id = null;
            }

            getSupportActionBar().setTitle(protegidoName);

            nombreTV.setText(protegidoName);

            FloatingActionButton agregarProtegido = (FloatingActionButton) findViewById(R.id.agregarProtectorEdit);
            agregarProtegido.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case DialogInterface.BUTTON_POSITIVE:
                                    writeNewUser();

                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(este);
                    builder.setMessage("¿Está seguro que desea realizar estos cambios?").setPositiveButton("Si", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener);
                    AlertDialog alert = builder.create();
                    alert.show();
                    alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);

                }
            });

            FloatingActionButton eliminarProtegido = (FloatingActionButton) findViewById(R.id.eliminarProtectorEdit);
            eliminarProtegido.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }catch(Throwable e){
            AlertDialog alertDialog=createAlertDialog("Ocurrió algo inesperado, por favor vuelva a intentarlo","Error");
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        }

    }


    private void writeNewUser() {
        try {

            try {
                if (id != null) {
                    DatabaseReference myRef1=database.getReference("usuariosAndroid");
                    myRef1.addValueEventListener(new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            Protegido value = dataSnapshot.getValue(Protegido.class);

                        }

                        @Override
                        public void onCancelled(DatabaseError error) {

                        }


                    });
                    user = new UsuarioProtegido(id, nombreTV.getText().toString(), telefono, correo, direccion,"Normal");
                    myRef.child(id).setValue(user);
                    finish();
                } else {

                    boolean connected = connectedToWiFi();
                    if (connected) {



                    DatabaseReference myRef1=database.getReference("usuariosAndroid");
                    myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(this!=null) {

                                v=false;
                                w=true;
                                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                    Protegido value = postSnapshot.getValue(Protegido.class);
                                    if (value.getId().equals(MainActivity.ID_DE_USUARIO_ACTUAL)) {
                                        actual = value;
                                    }
                                }for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                    Protegido value = postSnapshot.getValue(Protegido.class);
                                    if(idTV.getText().toString().equals(value.getCorreo())){
                                        v=true;
                                        idUserAgregar= value.getId();
                                        String key = myRef.push().getKey();
                                        user = new UsuarioProtegido(value.getId(), nombreTV.getText().toString(), value.getTelefono(), idTV.getText().toString(), value.getDireccion(),value.getEstado());
                                        Map<String, Object> postValues = user.toMap();
                                        correoUsuarioAgregar=user.getCorreo();

                                        childUpdates = new HashMap<>();
                                        childUpdates.put(key, postValues);
                                        HashMap<String,Object>hash=new HashMap<String,Object>();
                                        hash.put("telefono",value.getTelefono());
                                        hash.put("direccion",value.getDireccion());
                                        hash.put("correo",value.getCorreo());
                                        hash.put("nombre",user.getNombre());
                                        hash.put("id",value.getId());
                                        solicitud=new Solicitud(MainActivity.ID_DE_USUARIO_ACTUAL,value.getId(),"Protector",hash,value.getCorreo(),value.getNombre(),actual.getCorreo(),actual.getNombre());
                                        for(int i=0;i<listUsuariosProtegidos.size();i++){
                                            if(listUsuariosProtegidos.get(i).getCorreo().equals(value.getCorreo())){
                                                Log.d("dddddddddddddddddddd",listUsuariosProtegidos.get(i).getCorreo());
                                                w=false;
                                            }
                                        }

                                    }
                                }

                                if(w && v){
                                    ref.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if(this!=null) {
                                                boolean var=true;
                                                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                                    Solicitud value = postSnapshot.getValue(Solicitud.class);
                                                    if(value.getDe().equals(solicitud.getDe())&&value.getPara().equals(solicitud.getPara()) && value.getTipo().equals("Protector")){
                                                        var=false;
                                                    }
                                                }
                                                if(var){
                                                    protectoresRef= database.getReference("usuariosAndroid").child(idUserAgregar).child("protegidos");
                                                    UsuarioProtegido up=new UsuarioProtegido(actual);
                                                    String llave=protectoresRef.push().getKey();
                                                    HashMap<String,Object>protector=new HashMap<String, Object>();
                                                    protector.put(llave,up.toMap());
                                                    //protectoresRef.updateChildren(protector);
                                                    //myRef.updateChildren(childUpdates);
                                                    String key = ref.push().getKey();
                                                    HashMap<String, Object> mapa=new HashMap<String, Object>();
                                                    mapa.put(key,solicitud.toMap());
                                                    ref.updateChildren(mapa);
                                                    if(this !=null) {
                                                        try {
                                                            AlertDialog alertDialog = createAlertDialog("La información se actualizó satisfactoriamente", "¡Exitoso!");
                                                            alertDialog.show();
                                                            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                                                        }catch (Throwable e){

                                                        }
                                                    }

                                                }else{
                                                    try {
                                                        if (this != null) {
                                                            AlertDialog alertDialog = createAlertDialog("Ya ha enviado una solicitud a este contacto.", "¡Error!");
                                                            alertDialog.show();
                                                            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                                                        }
                                                    }catch (Throwable e){

                                                    }
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                                }
                                if(!v){
                                    try {
                                        if (this != null) {
                                            AlertDialog alertDialog = createAlertDialog("El usuario no existe en el sistema. Intente con un nuevo correo.", "¡Error!");
                                            alertDialog.show();
                                            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                                        }
                                    }catch(Throwable e){

                                    }
                                }
                                else if(!w){
                                    try {
                                        if (this != null) {
                                            AlertDialog alertDialog = createAlertDialog("El usuario ya hace parte de sus contactos.", "¡Error!");
                                            alertDialog.show();
                                            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                                        }
                                    }catch (Throwable e){

                                    }
                                }




                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    } else {
                        AlertDialog alertDialog=createAlertDialog("No estas conectado a internet, por lo tanto no puedes agregar contactos. Por favor conéctate y vuelve a intentarlo","Información");
                        alertDialog.show();
                        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                    }

                }


            } catch (Throwable e) {
                AlertDialog alertDialog = createAlertDialog("No se pudo actualizar este usuario, por favor intentelo de nuevo", "Error");
                alertDialog.show();
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
            }


            //  mDatabase.child("prueba");

            //mDatabase.child("usuariosAndroid").child(id).setValue(user);
        }catch(Throwable e){
            AlertDialog alertDialog=createAlertDialog("No se pudo actualizar este usuario, por favor intentelo de nuevo.","Error");
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        }

    }

    public AlertDialog createAlertDialog(String msg,String title){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);

        builder.setMessage(msg)
                .setTitle(title)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }

    private boolean connectedToWiFi(){
        ConnectivityManager connManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

}
