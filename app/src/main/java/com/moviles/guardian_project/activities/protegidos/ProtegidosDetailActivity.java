package com.moviles.guardian_project.activities.protegidos;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.moviles.guardian_project.MainActivity;
import com.moviles.guardian_project.R;
import com.moviles.guardian_project.activities.protectores.ProtectoresEditActivity;
import com.moviles.guardian_project.activities.protegidos.EditProtegidosActivity;
import com.moviles.guardian_project.entities.UsuarioProtegido;


public class ProtegidosDetailActivity extends AppCompatActivity {

    public final static int EDITAR=11;
    private static final String TAG="editar";



    private String nombre="p";
    private String correo="p";
    private String telefono="p";
    private String direccion="p";
    private String id="";
    private String idUsuario="";
    private TextView generoTV;

    private TextView telefonoTV;
    private TextView direccionTV;
    private TextView nombreTV;
    private TextView correoTV;

    private FirebaseDatabase database;

    private ProtegidosDetailActivity este;
    private DatabaseReference myRef;

    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_protegido_detail);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);


            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            este = this;
            if (getIntent() != null && getIntent().getExtras().getString("id") != null) {
                //idUsuario = getIntent().getExtras().getString("idUsuario");
                id = getIntent().getExtras().getString("id");

            }
            database = FirebaseDatabase.getInstance();
            myRef = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("protegidos");

            mDatabase = FirebaseDatabase.getInstance().getReference();

            correoTV = (TextView) findViewById(R.id.textViewCorreoProtegido);
            nombreTV = (TextView) findViewById(R.id.textViewNombreProtegido);
            direccionTV = (TextView) findViewById(R.id.textViewDireccionProtegido);
            telefonoTV = (TextView) findViewById(R.id.textViewTelefonoProtegido);

            if(id!=null && id!="") {

                getUser(id);
            }

            FloatingActionButton agregarProtegido = (FloatingActionButton) findViewById(R.id.agregarProtegido);
            agregarProtegido.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Intent i = new Intent(este, EditProtegidosActivity.class);

                    i.putExtra("nombre", nombre);
                    i.putExtra("correo", correo);
                    i.putExtra("telefono", telefono);
                    i.putExtra("direccion", direccion);
                    i.putExtra("id", id);
                    i.putExtra("idUsuario", MainActivity.ID_DE_USUARIO_ACTUAL);
                    startActivity(i);

                }
            });


            FloatingActionButton eliminarProtegido = (FloatingActionButton) findViewById(R.id.eliminarProtegido);
            eliminarProtegido.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    if (id != null) deleteUser(id);

                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(este);
                    builder.setMessage("¿Está seguro que desea eliminar a este usuario?").setPositiveButton("Si", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener);
                    AlertDialog alert = builder.create();
                    alert.show();
                    alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);

                }
            });

        }catch (Throwable e){
            AlertDialog alertDialog=createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo","Error");
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        }

    }

    private void getUser(String idN){
        if(this !=null) {
            try {

                myRef.child(idN).addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                            UsuarioProtegido value = dataSnapshot.getValue(UsuarioProtegido.class);
                            if (value != null) {

                                if (value.getNombre() != null) nombre = value.getNombre();
                                if (value.getCorreo() != null) correo = value.getCorreo();
                                if (value.getTelefono() != null)
                                    telefono = value.getTelefono() + "";
                                if (value.getDireccion() != null) direccion = value.getDireccion();

                                getSupportActionBar().setTitle(nombre);
                                correoTV.setText(correo);
                                nombreTV.setText(nombre);
                                direccionTV.setText(direccion);
                                telefonoTV.setText(telefono);

                            }


                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        Log.w(TAG, "Failed to read value.", error.toException());
                    }


                });
            } catch (Throwable e) {
                AlertDialog alertDialog = createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo", "Error");
                alertDialog.show();
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
            }
        }
    }

    private void deleteUser(String idN) {
        myRef.child(idN).setValue(null);
        AlertDialog alertDialog=createAlertDialog("El contacto se borró satisfactoriamente","¡Exitoso!");
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
    }

    public AlertDialog createAlertDialog(String msg,String title){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);

        builder.setMessage(msg)
                .setTitle(title)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }



    public void onResume(){
        try {
            super.onResume();
            if (id != null) getUser(id);
        }catch (Throwable e){
            AlertDialog alertDialog=createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo","Error");
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        }
    }
}
