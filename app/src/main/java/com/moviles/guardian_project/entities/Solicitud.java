package com.moviles.guardian_project.entities;

import java.util.HashMap;

/**
 * Created by Asus on 30/03/2017.
 */

public class Solicitud {
    private String de;
    private String para;
    private String tipo;
    private HashMap<String,Object> infoContacto;
    private String paraCorreo;
    private String paraNombre;
    private String deCorreo;
    private String deNombre;

    public Solicitud(String de, String para, String tipo, HashMap<String, Object> infoContacto,String paraCorreo,String paraNombre,String deCorreo,String deNombre) {
        this.de = de;
        this.para = para;
        this.tipo = tipo;
        this.infoContacto = infoContacto;
        this.paraCorreo=paraCorreo;
        this.paraNombre=paraNombre;
        this.deCorreo=deCorreo;
        this.deNombre=deNombre;
    }
    public Solicitud(){

    }

    public HashMap<String,Object>toMap(){
        HashMap<String,Object>result=new HashMap<String,Object>();
        result.put("de",de);
        result.put("para",para);
        result.put("tipo",tipo);
        result.put("infoContacto",infoContacto);
        result.put("paraCorreo",paraCorreo);
        result.put("paraNombre",paraNombre);
        result.put("deCorreo",deCorreo);
        result.put("deNombre",deNombre);
        return result;
    }

    public String getDe() {
        return de;
    }

    public void setDe(String de) {
        this.de = de;
    }

    public String getPara() {
        return para;
    }

    public void setPara(String para) {
        this.para = para;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public HashMap<String, Object> getInfoContacto() {
        return infoContacto;
    }

    public void setInfoContacto(HashMap<String, Object> infoContacto) {
        this.infoContacto = infoContacto;
    }

    public String getDeCorreo() {
        return deCorreo;
    }

    public void setDeCorreo(String deCorreo) {
        this.deCorreo = deCorreo;
    }

    public String getDeNombre() {
        return deNombre;
    }

    public void setDeNombre(String deNombre) {
        this.deNombre = deNombre;
    }

    public String getParaCorreo() {
        return paraCorreo;
    }

    public void setParaCorreo(String paraCorreo) {
        this.paraCorreo = paraCorreo;
    }

    public String getParaNombre() {
        return paraNombre;
    }

    public void setParaNombre(String paraNombre) {
        this.paraNombre = paraNombre;
    }
}
