package com.moviles.guardian_project.entities;

import com.google.gson.annotations.SerializedName;
import com.moviles.guardian_project.MainActivity;

import java.util.HashMap;

/**
 * Created by Asus on 22/04/2017.
 */

public class EmergenciaEnviadaFirebase {

    @SerializedName("notification")
    private HashMap<String,String> notification;

    @SerializedName("to")
    private String to;

    public EmergenciaEnviadaFirebase(String mensaje,String token) {
        to=token+"";
        notification=new HashMap<String,String>();
        notification.put("title","Emergencia Guardian");
        notification.put("body", MainActivity.yo.getNombre()+": "+mensaje);
        notification.put("color","#4E2AA5");
        notification.put("sound","default");
    }
}
