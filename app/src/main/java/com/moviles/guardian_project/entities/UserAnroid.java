package com.moviles.guardian_project.entities;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

/**
 * Created by Asus on 23/04/2017.
 */
@IgnoreExtraProperties
public class UserAnroid {
    private String nombre;
    private String cedula;
    private String correo;
    private String direccion;
    private String genero;
    private String id;
    private String fechaNacimiento;
    private Object emergenciasAtendidas;

    @Exclude
    private HashMap<String, UsuarioProtegido> protectores;

    @Exclude
    private HashMap<String, UsuarioProtegido> protegidos;

    private String telefono;
    private String tipoUsuario;
    private String token;
    private String estado;

    public UserAnroid(String cedula, String correo, String direccion, Object emergenciasAtendidas, String fechaNacimiento, String genero, String id, String nombre, HashMap<String, UsuarioProtegido> protectores, HashMap<String, UsuarioProtegido> protegidos, String telefono, String tipoUsuario, String token,String estado) {
        this.cedula = cedula;
        this.correo = correo;
        this.direccion = direccion;
        this.emergenciasAtendidas = emergenciasAtendidas;
        this.fechaNacimiento = fechaNacimiento;
        this.genero = genero;
        this.id = id;
        this.nombre = nombre;
        this.protectores = protectores;
        this.protegidos = protegidos;
        this.telefono = telefono;
        this.tipoUsuario = tipoUsuario;
        this.token = token;
        this.estado=estado;
    }
    public UserAnroid(){

    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Object getEmergenciasAtendidas() {
        return emergenciasAtendidas;
    }

    public void setEmergenciasAtendidas(Object emergenciasAtendidas) {
        this.emergenciasAtendidas = emergenciasAtendidas;
    }

    public HashMap<String, UsuarioProtegido> getProtegidos() {
        return protegidos;
    }

    public void setProtegidos(HashMap<String, UsuarioProtegido> protegidos) {
        this.protegidos = protegidos;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public HashMap<String, UsuarioProtegido> getProtectores() {
        return protectores;
    }

    public void setProtectores(HashMap<String, UsuarioProtegido> protectores) {
        this.protectores = protectores;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}