package com.moviles.guardian_project.entities;


import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.widget.TextView;

import com.moviles.guardian_project.R;
import com.moviles.guardian_project.fragmentos.GmapFragment;

/**
 * Created by Johan on 29/03/2017.
 */

public class GPSReceiver extends BroadcastReceiver{

    private FragmentManager fragmentManager;
    private ActionBar actionBar;
    private String latitud;
    private String longitud;



    public GPSReceiver(FragmentManager fm, ActionBar ab){
        this.fragmentManager=fm;
        this.actionBar=ab;
    }
    public void onReceive(Context context, Intent intent){

        latitud= intent.getStringExtra("latitud");
        longitud= intent.getStringExtra("longitud");

        GmapFragment gmap=new GmapFragment();

        fragmentManager.beginTransaction().replace(R.id.content_frame,gmap).commit();
        actionBar.setTitle("Guardian");

        Bundle data = new Bundle();
        data.putString("latitud", latitud);
        data.putString("longitud", longitud);
        gmap.setArguments(data);

    }
}
