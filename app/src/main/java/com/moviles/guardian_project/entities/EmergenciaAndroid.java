package com.moviles.guardian_project.entities;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Asus on 23/04/2017.
 */
@IgnoreExtraProperties
public class EmergenciaAndroid {

    private String estado;
    private String fecha;
    private String idUsuario;
    private Double latitud;
    private Double longitud;
    private String mensaje;
    private String tipo;

    public  EmergenciaAndroid(){

    }

    public EmergenciaAndroid(String estado, String fecha, String idUsuario, Double latitud, Double longitud, String mensaje, String tipo) {
        this.estado = estado;
        this.fecha = fecha;
        this.idUsuario = idUsuario;
        this.latitud = latitud;
        this.longitud = longitud;
        this.mensaje = mensaje;
        this.tipo = tipo;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("estado", estado);
        result.put("fecha", fecha);
        result.put("idUsuario", idUsuario);
        result.put("latitud", latitud);
        result.put("longitud", longitud);
        result.put("mensaje", mensaje);
        result.put("tipo", tipo);

        return result;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
