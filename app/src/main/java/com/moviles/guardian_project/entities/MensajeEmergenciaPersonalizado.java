package com.moviles.guardian_project.entities;

/**
 * Created by Asus on 19/02/2017.
 */

public class MensajeEmergenciaPersonalizado {
    private int idEmergencia;
    private String usuario;
    private String mensaje;

    public MensajeEmergenciaPersonalizado(String asunto, int idEmergencia, String mensaje) {
        this.usuario = asunto;
        this.idEmergencia = idEmergencia;
        this.mensaje = mensaje;
    }

    public String getUsuario() {
        return usuario;
    }

    public int getIdEmergencia() {
        return idEmergencia;
    }

    public String getMensaje() {
        return mensaje;
    }
}
