package com.moviles.guardian_project.entities;

/**
 * Created by Asus on 19/02/2017.
 */

public class Emergencia {
    private String idUsuario;
    private String estado;
    private String fecha;
    private double longitud;
    private double latitud;
    private String mensaje;
    private String tipo;


    public Emergencia(String idUsuario, String estado, String fecha, double longitud, double latitud, String mensaje, String tipo) {
        this.idUsuario = idUsuario;
        this.estado = estado;
        this.fecha = fecha;
        this.longitud = longitud;
        this.latitud = latitud;
        this.mensaje = mensaje;
        this.tipo = tipo;
    }

    public Emergencia() {
    }

    public String getidUsuario() {
        return idUsuario;
    }

    public String getestado() {
        return estado;
    }

    public String getfecha() {
        return fecha;
    }

    public double getlongitud() {
        return longitud;
    }

    public void setlongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
