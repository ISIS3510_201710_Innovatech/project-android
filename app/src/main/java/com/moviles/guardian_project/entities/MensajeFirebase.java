package com.moviles.guardian_project.entities;

import java.util.ArrayList;

/**
 * Created by Asus on 22/04/2017.
 */

public class MensajeFirebase {


    private Long multicast_id;
    private int succes;
    private int failure;
    private int canonical_ids;

    ArrayList<MessageId>messageIds;

    public MensajeFirebase(int canonical_ids, int failure, ArrayList<MessageId> messageIds, Long multicast_id, int succes) {
        this.canonical_ids = canonical_ids;
        this.failure = failure;
        this.messageIds = messageIds;
        this.multicast_id = multicast_id;
        this.succes = succes;

    }
}
