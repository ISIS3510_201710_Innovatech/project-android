package com.moviles.guardian_project.entities;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Asus on 25/04/2017.
 */
@IgnoreExtraProperties
public class EmergenciaAtendida {

    private String fecha;
    private String tipo;
    private String id;

    public EmergenciaAtendida(String fecha, String id, String tipo) {
        this.fecha = fecha;
        this.id = id;
        this.tipo = tipo;
    }

    public EmergenciaAtendida(){

    }
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("fecha", fecha);
        result.put("tipo", tipo);

        return result;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
