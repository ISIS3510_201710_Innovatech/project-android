package com.moviles.guardian_project.entities;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Johan on 11/03/2017.
 */
@IgnoreExtraProperties
public class Usuario {

    public String correo;
    public String fechaNacimiento;
    public String imagen;
    public String nombre;
    public Object protectores;
    public String username;

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int telefono;

    public Usuario() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Usuario(String correo, String fechaNacimiento, String imagen, String nombre, Object protectores, int telefono, String username) {
        this.correo = correo;
        this.fechaNacimiento = fechaNacimiento;
        this.imagen=imagen;
        this.nombre=nombre;
        this.protectores=protectores;
        this.username=username;
        this.telefono=telefono;

    }

    public Usuario(String correo, String fechaNacimiento, String imagen, String nombre, int telefono, String username) {
        this.correo = correo;
        this.fechaNacimiento = fechaNacimiento;
        this.imagen=imagen;
        this.nombre=nombre;
      //  this.protectores=(List)protectores;
        this.username=username;
        this.telefono=telefono;

    }

    public String toString(){
        return username+"-"+nombre;
    }


    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Object getProtectores() {
        return protectores;
    }


    public void setProtectores(Object protectores) {
        this.protectores = protectores;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }







}

