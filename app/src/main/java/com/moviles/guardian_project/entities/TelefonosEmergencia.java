package com.moviles.guardian_project.entities;

/**
 * Created by Asus on 23/03/2017.
 */

public class TelefonosEmergencia {

    private String imagen;
    private String nombre;
    private String telefonos;

    public TelefonosEmergencia(){

    }

    public TelefonosEmergencia(String imagen, String telefonos, String nombre) {
        this.imagen = imagen;
        this.telefonos = telefonos;
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(String telefonos) {
        this.telefonos = telefonos;
    }
}
