package com.moviles.guardian_project.entities;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Asus on 21/03/2017.
 */

@IgnoreExtraProperties
public class UsuarioProtegido {

    private String id;
    private String nombre;
    private String telefono;
    private String correo;
    private String direccion;
    private String estado;


    public UsuarioProtegido(String id, String nombre, String telefono, String correo, String direccion,String estado) {
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
        this.correo = correo;
        this.direccion = direccion;
        this.estado=estado;
    }

    public UsuarioProtegido(){

    }

    public UsuarioProtegido(Protegido protegido){
        this.id=protegido.getId();
        this.nombre=protegido.getNombre();
        this.telefono=protegido.getTelefono();
        this.correo=protegido.getCorreo();
        this.direccion=protegido.getDireccion();
        this.estado=protegido.getEstado();
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("nombre", nombre);
        result.put("telefono", telefono);
        result.put("correo", correo);
        result.put("direccion", direccion);
        result.put("estado",estado);

        return result;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
