package com.moviles.guardian_project.rest;

import com.moviles.guardian_project.entities.Emergencia;
import com.moviles.guardian_project.entities.EmergenciaEnviadaFirebase;
import com.moviles.guardian_project.entities.MensajeEmergenciaPersonalizado;
import com.moviles.guardian_project.entities.MensajeFirebase;
import com.moviles.guardian_project.entities.ReporteEmergenciaExitoso;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Asus on 19/02/2017.
 */

public interface ApiService {

    @Headers({"Content-Type: application/json",
            "Authorization: key=AAAAGFpNXD0:APA91bG2eTzZ8zu9mYzeBH8cFI3JNe8QCI0inJsz3_FhmzbXbxtzvdzuYrTJ-yZGwG5tZ3cIztpigra-vSHsW08h_qBCYhyK8sCdEumHHmJo2FuFFimyCgohh2ki4J9dSE9nqsuUm5T-"})

    @GET("/emergencias")
    Call<List<Emergencia>> getEmergencias();

    @POST("/reporte")
    Call<ReporteEmergenciaExitoso>createEmergencia(@Body MensajeEmergenciaPersonalizado mensajeEmergenciaPersonalizado);

    @Headers({"Content-Type: application/json",
    "Authorization: key=AAAAGFpNXD0:APA91bG2eTzZ8zu9mYzeBH8cFI3JNe8QCI0inJsz3_FhmzbXbxtzvdzuYrTJ-yZGwG5tZ3cIztpigra-vSHsW08h_qBCYhyK8sCdEumHHmJo2FuFFimyCgohh2ki4J9dSE9nqsuUm5T-"})
    @POST("/fcm/send")
    Call<MensajeFirebase> enviarNotificacion(@Body EmergenciaEnviadaFirebase emergenciaEnviadaFirebase);
}
