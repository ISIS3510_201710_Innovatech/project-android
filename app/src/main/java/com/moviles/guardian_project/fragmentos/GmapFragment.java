package com.moviles.guardian_project.fragmentos;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.LevelListDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.moviles.guardian_project.MainActivity;
import com.moviles.guardian_project.R;
import com.moviles.guardian_project.activities.protectores.ProtectoresEditActivity;
import com.moviles.guardian_project.entities.EmergenciaAndroid;
import com.moviles.guardian_project.entities.EmergenciaAtendida;
import com.moviles.guardian_project.entities.EmergenciaEnviadaFirebase;
import com.moviles.guardian_project.entities.MensajeFirebase;
import com.moviles.guardian_project.entities.Protegido;
import com.moviles.guardian_project.entities.UserAnroid;
import com.moviles.guardian_project.entities.UsuarioProtegido;
import com.moviles.guardian_project.rest.RestClient;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.moviles.guardian_project.entities.Emergencia;
import com.moviles.guardian_project.entities.UsuarioProtegido;
import com.moviles.guardian_project.services.Acelerometro;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 19/02/2017.
 */

public class GmapFragment extends Fragment implements OnMapReadyCallback {

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 4 0meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000*2; // 1000 * 10 seconds

    private Bitmap smallMarker;
    private String llave = "";
    private DatabaseReference re;
    private EditText editText;
    private String tipo;
    private String mensaje = "Ayudame, estoy en una emergencia";
    private static final String TAG = "FRAGMENT GMAP";
    private GoogleMap map;
    private double longitud = -74.0645732;
    private double latitud = 4.6028383;
    private boolean estadoEmergencia = false;

    private DatabaseReference myRef;
    private List<Emergencia> listEmergencias;
    LocationListener locationListener;
    Location mLastLocation;
    LocationManager locationManager;
    private String mensajeSalud = "Ayudame, estoy en una emergencia";
    private Map<String, Object> childUpdates;

    private FirebaseDatabase database;
    private DatabaseReference myRefEmergency;

    private Intent servicio;
    private boolean enEmergenciaMaxima = false;
    FloatingActionButton emergenciaMaxima;
    private Marker markers[];
    private Marker marker2;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;


        DatabaseReference myRef1 = database.getReference("usuariosAndroid");
        myRef1.keepSynced(true);
        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (getActivity() != null) {

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        UserAnroid u = postSnapshot.getValue(UserAnroid.class);
                        if(u.getTipoUsuario().equals("Policia") && u.getId().equals(MainActivity.ID_DE_USUARIO_ACTUAL)) {

                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("emergenciasAndroid");
                            myRefEmergency.keepSynced(true);
                            EmergenciaAndroid emergenciaAndroid = new EmergenciaAndroid("0", (new Date()).toString(), u.getId(), latitud, longitud, "Policia nacional", "Policia");
                            HashMap<String, Object> protector = new HashMap<String, Object>();
                            protector.put(u.getId(), emergenciaAndroid.toMap());
                            childUpdates = protector;
                            myRefEmergency.updateChildren(childUpdates);
                        }
                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        getListaEmergencias();


        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;

        } else {


            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            mLastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (mLastLocation == null)
                mLastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (mLastLocation == null)
                mLastLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if (mLastLocation == null) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
                Log.e(TAG, "LOCATION UPDATES ACTIVADAS3");

            }
           /* if (locationManager != null) {
                Log.e(TAG, "LOCATION UPDATES DESACTIVADAS");
                locationManager.removeUpdates(locationListener);
            }*/


            if (mLastLocation != null) {
                Log.e(TAG, "posicion= " + mLastLocation.getLatitude() + " / " + mLastLocation.getLongitude());
                latitud = mLastLocation.getLatitude();
                longitud = mLastLocation.getLongitude();
                LatLng currentLatlong = new LatLng(latitud, longitud);

                map.setMyLocationEnabled(true);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatlong, 16));


            } else {
                Log.e(TAG, "No se pudo usar el GPS ");
                AlertDialog alertDialog = createAlertDialog("Para acceder a la posición active el GPS.", "¡No está activado el GPS!");
                alertDialog.show();
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);

            }


        }


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int height = 100;
        int width = 100;


        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("emergenciasAndroid");

        listEmergencias = new ArrayList<Emergencia>();

        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates

        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                Location temp = mLastLocation;
                mLastLocation = location;
                latitud = location.getLatitude();
                longitud = location.getLongitude();
                if (enEmergenciaMaxima) {
                    myRef.child(MainActivity.ID_DE_USUARIO_ACTUAL).child("latitud").setValue(latitud);
                    myRef.child(MainActivity.ID_DE_USUARIO_ACTUAL).child("longitud").setValue(longitud);
                    Log.e(TAG, "Enviando localizacion emergencia " + latitud + ": " + longitud);
                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };


        if (connectedToWiFi3()) {
            database = FirebaseDatabase.getInstance();
            DatabaseReference myRef1 = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL);
            myRef1.keepSynced(true);
            myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (getActivity() != null) {

                        UserAnroid u = dataSnapshot.getValue(UserAnroid.class);
                        if (u.getEstado().equals("Normal")) {
                            RelativeLayout protegidoNuevo = (RelativeLayout) getActivity().findViewById(R.id.mapemerg);
                            protegidoNuevo.setVisibility(View.INVISIBLE);
                            estadoEmergencia = false;
                            enEstadoNormal();

                        } else {
                            RelativeLayout protegidoNuevo = (RelativeLayout) getActivity().findViewById(R.id.mapemerg);
                            protegidoNuevo.setVisibility(View.VISIBLE);
                            estadoEmergencia = true;
                            enEstadoEmergencia();
                        }

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            RelativeLayout protegidoNuevo = (RelativeLayout) getActivity().findViewById(R.id.mapemerg);
            protegidoNuevo.setVisibility(View.INVISIBLE);
            estadoEmergencia = false;
            enEstadoNormal();
        }

        FloatingActionButton nuevoProtector = (FloatingActionButton) view.findViewById(R.id.fabemerg);
        nuevoProtector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                database = FirebaseDatabase.getInstance();
                database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("estado").setValue("Normal");

                actualizarEstadoANormal();
                RelativeLayout protegidoNuevo = (RelativeLayout) getActivity().findViewById(R.id.mapemerg);
                protegidoNuevo.setVisibility(View.INVISIBLE);
                estadoEmergencia = false;
                enEstadoNormal();
            }
        });


        emergenciaMaxima = (FloatingActionButton) view.findViewById(R.id.emergenciaMax);
        if (enEmergenciaMaxima)
            emergenciaMaxima.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
        else emergenciaMaxima.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryB)));

        emergenciaMaxima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null) {
                    if (!enEmergenciaMaxima) {
                        enEmergenciaMaxima = true;
                        emergenciaMaxima.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                        servicio = new Intent(getActivity(), Acelerometro.class);
                        servicio.putExtra("userId", MainActivity.ID_DE_USUARIO_ACTUAL);
                        getActivity().startService(servicio);

                    } else {
                        enEmergenciaMaxima = false;
                        emergenciaMaxima.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryB)));
                        if (servicio != null) getActivity().stopService(servicio);

                    }

                }
            }
        });


        MapFragment fragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.mapp);
        fragment.getMapAsync(this);

        FloatingActionButton nuevoProtector2 = (FloatingActionButton) view.findViewById(R.id.b1);
        nuevoProtector2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // custom dialog

                LayoutInflater factory = LayoutInflater.from(getActivity());
                final View deleteDialogView = factory.inflate(R.layout.dialog_emergency, null);
                final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();

                editText = (EditText) deleteDialogView.findViewById(R.id.editTextDialog);
                ImageView im = (ImageView) deleteDialogView.findViewById(R.id.ime);
                im.setImageResource(R.drawable.docicon);
                deleteDialog.setView(deleteDialogView);
                deleteDialog.getWindow().setBackgroundDrawableResource(R.color.colorPrimary);
                //ImageView image = (ImageView) deleteDialog.findViewById();
                //image.setImageResource(R.drawable.docicon);
                deleteDialogView.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mensajeSalud = !editText.getText().toString().equals("") ? editText.getText().toString() : "Ayudame, estoy en una emergencia";

                        if (connectedToWiFi3()) {
                            //your business logic
                            RelativeLayout protegidoNuevo = (RelativeLayout) getActivity().findViewById(R.id.mapemerg);
                            protegidoNuevo.setVisibility(View.VISIBLE);
                            estadoEmergencia = true;
                            enEstadoEmergencia();

                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("emergenciasAndroid");
                            myRefEmergency.keepSynced(true);
                            EmergenciaAndroid emergenciaAndroid = new EmergenciaAndroid("0", (new Date()).toString(), MainActivity.ID_DE_USUARIO_ACTUAL, latitud, longitud, mensajeSalud, "Salud");
                            HashMap<String, Object> protector = new HashMap<String, Object>();
                            protector.put(MainActivity.ID_DE_USUARIO_ACTUAL, emergenciaAndroid.toMap());
                            childUpdates = protector;
                            myRefEmergency.updateChildren(childUpdates);

                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("emergenciasReportadas");
                            HashMap<String, Object> protector2 = new HashMap<String, Object>();
                            protector2.put(myRefEmergency.push().getKey(), emergenciaAndroid.toMap());
                            childUpdates = protector2;
                            myRefEmergency.updateChildren(childUpdates);


                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("estado");
                            myRefEmergency.setValue("Emergencia");

                            enviarNotificacion("Salud", mensajeSalud);

                            AlertDialog alert = createAlertDialog("Tu emergencia fue reportada satisfactoriamente", "Exitosa");
                            alert.show();
                            alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                            deleteDialog.dismiss();
                        } else {
                            //sendSMS("3214869283","Mensaje Guardian: Salud - "+mensaje);
                            EmergenciaAndroid emergenciaAndroid = new EmergenciaAndroid("0", (new Date()).toString(), MainActivity.ID_DE_USUARIO_ACTUAL, latitud, longitud, mensajeSalud, "Salud");

                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("emergenciasReportadas");
                            HashMap<String, Object> protector2 = new HashMap<String, Object>();
                            protector2.put(myRefEmergency.push().getKey(), emergenciaAndroid.toMap());
                            childUpdates = protector2;
                            myRefEmergency.updateChildren(childUpdates);

                            database = FirebaseDatabase.getInstance();
                            DatabaseReference myRef1 = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("protectores");
                            myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (this != null) {

                                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                            try {
                                                UsuarioProtegido value = postSnapshot.getValue(UsuarioProtegido.class);
                                                sendSMS(value.getTelefono(), "Mensaje Guardian: Salud - " + mensajeSalud);

                                            } catch (Throwable e) {

                                            }

                                        }

                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });


                            AlertDialog alert = createAlertDialog("No estas conectado a internet, se enviaron mensajes SMS a tus protectores", "Atención");
                            alert.show();
                            alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                            deleteDialog.dismiss();
                        }
                    }
                });
                deleteDialogView.findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteDialog.dismiss();
                    }
                });

                deleteDialog.show();


            }
        });


        nuevoProtector2 = (FloatingActionButton) view.findViewById(R.id.b3);
        nuevoProtector2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // custom dialog

                LayoutInflater factory = LayoutInflater.from(getActivity());
                final View deleteDialogView = factory.inflate(R.layout.dialog_emergency, null);
                final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();

                editText = (EditText) deleteDialogView.findViewById(R.id.editTextDialog);
                ImageView im = (ImageView) deleteDialogView.findViewById(R.id.ime);
                im.setImageResource(R.drawable.caricon);
                deleteDialog.setView(deleteDialogView);
                deleteDialog.getWindow().setBackgroundDrawableResource(R.color.colorBackground);
                //ImageView image = (ImageView) deleteDialog.findViewById();
                //image.setImageResource(R.drawable.docicon);
                deleteDialogView.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mensajeSalud = !editText.getText().toString().equals("") ? editText.getText().toString() : "Ayudame, estoy en una emergencia";

                        if (connectedToWiFi3()) {
                            //your business logic
                            RelativeLayout protegidoNuevo = (RelativeLayout) getActivity().findViewById(R.id.mapemerg);
                            protegidoNuevo.setVisibility(View.VISIBLE);
                            estadoEmergencia = true;
                            enEstadoEmergencia();

                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("emergenciasAndroid");
                            myRefEmergency.keepSynced(true);
                            EmergenciaAndroid emergenciaAndroid = new EmergenciaAndroid("0", (new Date()).toString(), MainActivity.ID_DE_USUARIO_ACTUAL, latitud, longitud, mensajeSalud, "Tránsito");
                            HashMap<String, Object> protector = new HashMap<String, Object>();
                            protector.put(MainActivity.ID_DE_USUARIO_ACTUAL, emergenciaAndroid.toMap());
                            childUpdates = protector;
                            myRefEmergency.updateChildren(childUpdates);

                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("emergenciasReportadas");
                            HashMap<String, Object> protector2 = new HashMap<String, Object>();
                            protector2.put(myRefEmergency.push().getKey(), emergenciaAndroid.toMap());
                            childUpdates = protector2;
                            myRefEmergency.updateChildren(childUpdates);


                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("estado");
                            myRefEmergency.setValue("Emergencia");

                            enviarNotificacion("Tránsito", mensajeSalud);

                            AlertDialog alert = createAlertDialog("Tu emergencia fue reportada satisfactoriamente", "Exitosa");
                            alert.show();
                            alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                            deleteDialog.dismiss();
                        } else {
                            //sendSMS("3214869283","Mensaje Guardian: Salud - "+mensaje);
                            EmergenciaAndroid emergenciaAndroid = new EmergenciaAndroid("0", (new Date()).toString(), MainActivity.ID_DE_USUARIO_ACTUAL, latitud, longitud, mensajeSalud, "Salud");

                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("emergenciasReportadas");
                            HashMap<String, Object> protector2 = new HashMap<String, Object>();
                            protector2.put(myRefEmergency.push().getKey(), emergenciaAndroid.toMap());
                            childUpdates = protector2;
                            myRefEmergency.updateChildren(childUpdates);

                            database = FirebaseDatabase.getInstance();
                            DatabaseReference myRef1 = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("protectores");
                            myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (this != null) {

                                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                            try {
                                                UsuarioProtegido value = postSnapshot.getValue(UsuarioProtegido.class);
                                                sendSMS(value.getTelefono(), "Mensaje Guardian: Tránsito - " + mensajeSalud);

                                            } catch (Throwable e) {

                                            }

                                        }

                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });


                            AlertDialog alert = createAlertDialog("No estas conectado a internet, se enviaron mensajes SMS a tus protectores", "Atención");
                            alert.show();
                            alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                            deleteDialog.dismiss();
                        }
                    }
                });
                deleteDialogView.findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteDialog.dismiss();
                    }
                });

                deleteDialog.show();


            }
        });


        nuevoProtector2 = (FloatingActionButton) view.findViewById(R.id.b4);
        nuevoProtector2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // custom dialog

                LayoutInflater factory = LayoutInflater.from(getActivity());
                final View deleteDialogView = factory.inflate(R.layout.dialog_emergency, null);
                final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();

                editText = (EditText) deleteDialogView.findViewById(R.id.editTextDialog);
                ImageView im = (ImageView) deleteDialogView.findViewById(R.id.ime);
                im.setImageResource(R.drawable.fire22);
                deleteDialog.setView(deleteDialogView);
                deleteDialog.getWindow().setBackgroundDrawableResource(R.color.com_facebook_blue);
                //ImageView image = (ImageView) deleteDialog.findViewById();
                //image.setImageResource(R.drawable.docicon);
                deleteDialogView.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mensajeSalud = !editText.getText().toString().equals("") ? editText.getText().toString() : "Ayudame, estoy en una emergencia";

                        if (connectedToWiFi3()) {
                            //your business logic
                            RelativeLayout protegidoNuevo = (RelativeLayout) getActivity().findViewById(R.id.mapemerg);
                            protegidoNuevo.setVisibility(View.VISIBLE);
                            estadoEmergencia = true;
                            enEstadoEmergencia();

                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("emergenciasAndroid");
                            myRefEmergency.keepSynced(true);
                            EmergenciaAndroid emergenciaAndroid = new EmergenciaAndroid("0", (new Date()).toString(), MainActivity.ID_DE_USUARIO_ACTUAL, latitud, longitud, mensajeSalud, "Incendio");
                            HashMap<String, Object> protector = new HashMap<String, Object>();
                            protector.put(MainActivity.ID_DE_USUARIO_ACTUAL, emergenciaAndroid.toMap());
                            childUpdates = protector;
                            myRefEmergency.updateChildren(childUpdates);

                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("emergenciasReportadas");
                            HashMap<String, Object> protector2 = new HashMap<String, Object>();
                            protector2.put(myRefEmergency.push().getKey(), emergenciaAndroid.toMap());
                            childUpdates = protector2;
                            myRefEmergency.updateChildren(childUpdates);


                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("estado");
                            myRefEmergency.setValue("Emergencia");

                            enviarNotificacion("Incendio", mensajeSalud);

                            AlertDialog alert = createAlertDialog("Tu emergencia fue reportada satisfactoriamente", "Exitosa");
                            alert.show();
                            alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                            deleteDialog.dismiss();
                        } else {
                            //sendSMS("3214869283","Mensaje Guardian: Salud - "+mensaje);
                            EmergenciaAndroid emergenciaAndroid = new EmergenciaAndroid("0", (new Date()).toString(), MainActivity.ID_DE_USUARIO_ACTUAL, latitud, longitud, mensajeSalud, "Salud");

                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("emergenciasReportadas");
                            HashMap<String, Object> protector2 = new HashMap<String, Object>();
                            protector2.put(myRefEmergency.push().getKey(), emergenciaAndroid.toMap());
                            childUpdates = protector2;
                            myRefEmergency.updateChildren(childUpdates);

                            database = FirebaseDatabase.getInstance();
                            DatabaseReference myRef1 = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("protectores");
                            myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (this != null) {

                                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                            try {
                                                UsuarioProtegido value = postSnapshot.getValue(UsuarioProtegido.class);
                                                sendSMS(value.getTelefono(), "Mensaje Guardian: Incendio - " + mensajeSalud);

                                            } catch (Throwable e) {

                                            }

                                        }

                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });


                            AlertDialog alert = createAlertDialog("No estas conectado a internet, se enviaron mensajes SMS a tus protectores", "Atención");
                            alert.show();
                            alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                            deleteDialog.dismiss();
                        }
                    }
                });
                deleteDialogView.findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteDialog.dismiss();
                    }
                });

                deleteDialog.show();


            }
        });


        nuevoProtector2 = (FloatingActionButton) view.findViewById(R.id.b2);
        nuevoProtector2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // custom dialog

                LayoutInflater factory = LayoutInflater.from(getActivity());
                final View deleteDialogView = factory.inflate(R.layout.dialog_emergency, null);
                final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();

                editText = (EditText) deleteDialogView.findViewById(R.id.editTextDialog);
                ImageView im = (ImageView) deleteDialogView.findViewById(R.id.ime);
                im.setImageResource(R.drawable.crimeicon);
                deleteDialog.setView(deleteDialogView);
                deleteDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                //ImageView image = (ImageView) deleteDialog.findViewById();
                //image.setImageResource(R.drawable.docicon);
                deleteDialogView.findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mensajeSalud = !editText.getText().toString().equals("") ? editText.getText().toString() : "Ayudame, estoy en una emergencia";

                        if (connectedToWiFi3()) {
                            RelativeLayout protegidoNuevo = (RelativeLayout) getActivity().findViewById(R.id.mapemerg);
                            protegidoNuevo.setVisibility(View.VISIBLE);
                            estadoEmergencia = true;
                            enEstadoEmergencia();

                            //your business logic
                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("emergenciasAndroid");
                            myRefEmergency.keepSynced(true);
                            EmergenciaAndroid emergenciaAndroid = new EmergenciaAndroid("0", (new Date()).toString(), MainActivity.ID_DE_USUARIO_ACTUAL, latitud, longitud, mensajeSalud, "Robo");
                            HashMap<String, Object> protector = new HashMap<String, Object>();
                            protector.put(MainActivity.ID_DE_USUARIO_ACTUAL, emergenciaAndroid.toMap());
                            childUpdates = protector;
                            myRefEmergency.updateChildren(childUpdates);

                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("emergenciasReportadas");
                            HashMap<String, Object> protector2 = new HashMap<String, Object>();
                            protector2.put(myRefEmergency.push().getKey(), emergenciaAndroid.toMap());
                            childUpdates = protector2;
                            myRefEmergency.updateChildren(childUpdates);


                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("estado");
                            myRefEmergency.setValue("Emergencia");

                            enviarNotificacion("Robo", mensajeSalud);

                            AlertDialog alert = createAlertDialog("Tu emergencia fue reportada satisfactoriamente", "Exitosa");
                            alert.show();
                            alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                            deleteDialog.dismiss();
                        } else {
                            //sendSMS("3214869283","Mensaje Guardian: Salud - "+mensaje);
                            EmergenciaAndroid emergenciaAndroid = new EmergenciaAndroid("0", (new Date()).toString(), MainActivity.ID_DE_USUARIO_ACTUAL, latitud, longitud, mensajeSalud, "Salud");

                            database = FirebaseDatabase.getInstance();
                            myRefEmergency = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("emergenciasReportadas");
                            HashMap<String, Object> protector2 = new HashMap<String, Object>();
                            protector2.put(myRefEmergency.push().getKey(), emergenciaAndroid.toMap());
                            childUpdates = protector2;
                            myRefEmergency.updateChildren(childUpdates);

                            database = FirebaseDatabase.getInstance();
                            DatabaseReference myRef1 = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("protectores");
                            myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (this != null) {

                                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                            try {
                                                UsuarioProtegido value = postSnapshot.getValue(UsuarioProtegido.class);
                                                sendSMS(value.getTelefono(), "Mensaje Guardian: Robo - " + mensajeSalud);

                                            } catch (Throwable e) {

                                            }

                                        }

                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });


                            AlertDialog alert = createAlertDialog("No estas conectado a internet, se enviaron mensajes SMS a tus protectores", "Atención");
                            alert.show();
                            alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                            deleteDialog.dismiss();
                        }
                    }
                });
                deleteDialogView.findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteDialog.dismiss();
                    }
                });

                deleteDialog.show();


            }
        });

    }


    private void sendSMS(String phoneNumber, String message) {
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, null, null);
    }

    public void actualizarEstadoAPeligro() {
        database = FirebaseDatabase.getInstance();
        DatabaseReference myRef1 = database.getReference("usuariosAndroid");
        myRef1.keepSynced(true);
        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (this != null) {

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        try {
                            UserAnroid value = postSnapshot.getValue(UserAnroid.class);
                            database = FirebaseDatabase.getInstance();
                            HashMap<String, Object> mapa = (new HashMap<String, Object>());
                            mapa.put("estado", "Emergencia");
                            for (String usuario : value.getProtegidos().keySet()) {
                                if (usuario.equals(MainActivity.ID_DE_USUARIO_ACTUAL)) {
                                    Log.e("0090908080808080800", database.getReference("usuariosAndroid").child(value.getId()).child("protegidos").child(MainActivity.ID_DE_USUARIO_ACTUAL).getKey());

                                    database.getReference("usuariosAndroid").child(value.getId()).child("protegidos").child(MainActivity.ID_DE_USUARIO_ACTUAL).updateChildren(mapa);


                                }
                            }


                        } catch (Throwable e) {

                        }

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void actualizarEstadoANormal() {

        database = FirebaseDatabase.getInstance();
        DatabaseReference myRef4 = database.getReference("emergenciasAndroid");
        myRef4.child(MainActivity.ID_DE_USUARIO_ACTUAL).removeValue();

        database = FirebaseDatabase.getInstance();
        DatabaseReference myRef1 = database.getReference("usuariosAndroid");
        myRef1.keepSynced(true);
        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (this != null) {

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        try {
                            UserAnroid value = postSnapshot.getValue(UserAnroid.class);
                            database = FirebaseDatabase.getInstance();
                            HashMap<String, Object> mapa = (new HashMap<String, Object>());
                            mapa.put("estado", "Normal");
                            for (String usuario : value.getProtegidos().keySet()) {
                                if (usuario.equals(MainActivity.ID_DE_USUARIO_ACTUAL)) {
                                    Log.e("0090908080808080800", database.getReference("usuariosAndroid").child(value.getId()).child("protegidos").child(MainActivity.ID_DE_USUARIO_ACTUAL).getKey());

                                    database.getReference("usuariosAndroid").child(value.getId()).child("protegidos").child(MainActivity.ID_DE_USUARIO_ACTUAL).updateChildren(mapa);


                                }
                            }


                        } catch (Throwable e) {

                        }

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public void enviarNotAyuda(String t, String m,String id){
        mensaje = m;
        tipo = t;
        database = FirebaseDatabase.getInstance();
        DatabaseReference myRef1 = database.getReference("usuariosAndroid").child(id);

        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (this != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        if (postSnapshot.getKey().equals("token")) {

                            try {
                                Log.e("1121212121212", postSnapshot.toString());
                                EmergenciaEnviadaFirebase emergenciaEnviadaFirebase = new EmergenciaEnviadaFirebase(tipo + " - " + mensaje, postSnapshot.getValue(String.class));
                                Call<MensajeFirebase> call = RestClient.getInstance().getApiService().enviarNotificacion(emergenciaEnviadaFirebase);

                                call.enqueue(new Callback<MensajeFirebase>() {
                                    @Override
                                    public void onResponse(Call<MensajeFirebase> call, Response<MensajeFirebase> response) {
                                        Log.e("111111111111", response.code() + "");
                                        /*Log.e("5555555555555",call.request().toString());
                                        Log.e("111111111111", response.code()+"");
                                        Log.e("888888888",call.request().headers().toString());
                                        try {
                                            final Buffer buffer = new Buffer();

                                                call.request().body().writeTo(buffer);

                                            Log.e("5555555555555",buffer.readUtf8());
                                        } catch (final IOException e) {
                                            Log.e("5555555555555", "did not work");
                                        }
                                    */
                                    }

                                    @Override
                                    public void onFailure(Call<MensajeFirebase> call, Throwable t) {
                                        Log.e("5555555555555", call.request().toString());
                                    }
                                });
                            } catch (Throwable e) {

                            }
                        }

                    }



                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    public void enviarNotificacion(String t, String m) {
        actualizarEstadoAPeligro();
        mensaje = m;
        tipo = t;


        database = FirebaseDatabase.getInstance();
        DatabaseReference myRef1 = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("protectores");
        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (this != null) {

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        try {
                            UsuarioProtegido value = postSnapshot.getValue(UsuarioProtegido.class);
                            database = FirebaseDatabase.getInstance();
                            DatabaseReference myRef2 = database.getReference("usuariosAndroid").child(value.getId());
                            myRef2.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (this != null) {

                                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                            if (postSnapshot.getKey().equals("token")) {

                                                try {
                                                    Log.e("1121212121212", postSnapshot.toString());
                                                    EmergenciaEnviadaFirebase emergenciaEnviadaFirebase = new EmergenciaEnviadaFirebase(tipo + " - " + mensaje, postSnapshot.getValue(String.class));
                                                    Call<MensajeFirebase> call = RestClient.getInstance().getApiService().enviarNotificacion(emergenciaEnviadaFirebase);

                                                    call.enqueue(new Callback<MensajeFirebase>() {
                                                        @Override
                                                        public void onResponse(Call<MensajeFirebase> call, Response<MensajeFirebase> response) {
                                                            Log.e("111111111111", response.code() + "");
                                        /*Log.e("5555555555555",call.request().toString());
                                        Log.e("111111111111", response.code()+"");
                                        Log.e("888888888",call.request().headers().toString());
                                        try {
                                            final Buffer buffer = new Buffer();

                                                call.request().body().writeTo(buffer);

                                            Log.e("5555555555555",buffer.readUtf8());
                                        } catch (final IOException e) {
                                            Log.e("5555555555555", "did not work");
                                        }
                                    */
                                                        }

                                                        @Override
                                                        public void onFailure(Call<MensajeFirebase> call, Throwable t) {
                                                            Log.e("5555555555555", call.request().toString());
                                                        }
                                                    });
                                                } catch (Throwable e) {

                                                }
                                            }

                                        }

                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                        } catch (Throwable e) {

                        }

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        database = FirebaseDatabase.getInstance();
        myRef1 = database.getReference("usuariosAndroid");
        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (this != null) {

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        try {
                            EmergenciaEnviadaFirebase emergenciaEnviadaFirebase = new EmergenciaEnviadaFirebase(tipo + " - " + mensaje, postSnapshot.getValue(UserAnroid.class).getToken());
                            Call<MensajeFirebase> call = RestClient.getInstance().getApiService().enviarNotificacion(emergenciaEnviadaFirebase);

                            call.enqueue(new Callback<MensajeFirebase>() {
                                @Override
                                public void onResponse(Call<MensajeFirebase> call, Response<MensajeFirebase> response) {
                                    Log.e("111111111111", response.code() + "");
                                        /*Log.e("5555555555555",call.request().toString());
                                        Log.e("111111111111", response.code()+"");
                                        Log.e("888888888",call.request().headers().toString());
                                        try {
                                            final Buffer buffer = new Buffer();

                                                call.request().body().writeTo(buffer);

                                            Log.e("5555555555555",buffer.readUtf8());
                                        } catch (final IOException e) {
                                            Log.e("5555555555555", "did not work");
                                        }
                                    */
                                }

                                @Override
                                public void onFailure(Call<MensajeFirebase> call, Throwable t) {
                                    Log.e("5555555555555", call.request().toString());
                                }
                            });
                        } catch (Throwable e) {

                        }

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        try {
            return inflater.inflate(R.layout.fragment_gmaps, container, false);
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }


    private AlertDialog createAlertDialog(String msg, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(msg)
                .setTitle(title)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }


    private boolean connectedToWiFi3() {
        ConnectivityManager connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }


    private void getListaEmergencias() {
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listEmergencias = new ArrayList<Emergencia>();
                enEmergenciaMaxima=false;
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Emergencia value = postSnapshot.getValue(Emergencia.class);
                    listEmergencias.add(value);
                    Log.e(TAG, "Uusarios en emergencia: id " + value.getidUsuario() + " loc " + value.getLatitud() + " * " + value.getlongitud());
                    if(MainActivity.ID_DE_USUARIO_ACTUAL!=null && value!=null && value.getidUsuario()!=null && value.getidUsuario().equals(MainActivity.ID_DE_USUARIO_ACTUAL)){
                        enEmergenciaMaxima=true;
                    }
                }

                try {
                    if(enEmergenciaMaxima){
                        emergenciaMaxima.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                        RelativeLayout protegidoNuevo = (RelativeLayout) getActivity().findViewById(R.id.mapemerg);
                        protegidoNuevo.setVisibility(View.VISIBLE);

                    }
                    else emergenciaMaxima.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryB)));

                }catch(Exception e){

                }

                LatLng posicionM;


                if (map != null) {
                    map.clear();


                    markers=new Marker[listEmergencias.size()];
                    for (int i = 0; i < listEmergencias.size(); i++) {
                        posicionM = new LatLng(listEmergencias.get(i).getLatitud(), listEmergencias.get(i).getlongitud());
                        Integer cont=0;
                        try {
                            cont = Integer.parseInt(listEmergencias.get(i).getestado());
                            if(listEmergencias.get(i).getTipo().equals("Policia")){
                                map.addMarker(new MarkerOptions().position(posicionM)
                                        .title(listEmergencias.get(i).getTipo()+"- "+listEmergencias.get(i).getMensaje())
                                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                            }else{
                                if(cont>10){
                                    markers[i]=map.addMarker(new MarkerOptions().position(posicionM)
                                            .title(listEmergencias.get(i).getTipo()+"- "+listEmergencias.get(i).getMensaje())
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                                    markers[i].setTag(listEmergencias.get(i));
                                }else{
                                    markers[i]=map.addMarker(new MarkerOptions().position(posicionM)
                                            .title(listEmergencias.get(i).getTipo()+"- "+listEmergencias.get(i).getMensaje())
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                                    markers[i].setTag(listEmergencias.get(i));
                                }
                            }

                        }catch (Throwable e){
                            markers[i]=map.addMarker(new MarkerOptions().position(posicionM)
                                    .title(listEmergencias.get(i).getTipo()+"- "+listEmergencias.get(i).getMensaje())
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                            markers[i].setTag(listEmergencias.get(i));
                        }

                    }

                    map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            marker2=marker;
                            LayoutInflater factory = LayoutInflater.from(getActivity());
                            final View deleteDialogView = factory.inflate(R.layout.dialog_atender_emergencia, null);
                            final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();

                            deleteDialog.setView(deleteDialogView);
                            deleteDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                            deleteDialogView.findViewById(R.id.ayudar).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (connectedToWiFi3()) {
                                        //your business logic
                                        enviarNotAyuda("Tu emergencia fue escuchada por alguien", MainActivity.CORREO_DE_USUARIO_ACTUAL+" atenderá tu emergencia",((Emergencia)marker2.getTag()).getidUsuario());
                                        database = FirebaseDatabase.getInstance();
                                        DatabaseReference myRef1 = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL).child("emergenciasAtendidas");
                                        String key = myRef1.push().getKey();
                                        EmergenciaAtendida e=new EmergenciaAtendida((new Date()).toString(),key,((Emergencia)marker2.getTag()).getTipo());
                                        Map<String, Object> postValues = e.toMap();
                                        HashMap<String, Object> mapa=new HashMap<String, Object>();
                                        mapa.put(key,postValues);
                                        myRef1.updateChildren(mapa);



                                        AlertDialog alert = createAlertDialog("Tu ayuda fue reportada satisfactoriamente", "Exitosa");
                                        alert.show();
                                        alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                                        deleteDialog.dismiss();
                                    } else {
                                        //sendSMS("3214869283","Mensaje Guardian: Salud - "+mensaje);


                                        database = FirebaseDatabase.getInstance();
                                        DatabaseReference myRef1 = database.getReference("usuariosAndroid").child(((Emergencia)marker2.getTag()).getidUsuario());
                                        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                if (this != null) {

                                                        try {
                                                            Protegido value = dataSnapshot.getValue(Protegido.class);
                                                            sendSMS(value.getTelefono(), "La ayuda viene en camino");

                                                        } catch (Throwable e) {

                                                        }



                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });


                                        AlertDialog alert = createAlertDialog("No estas conectado a internet, se envió un mensaje SMS", "Atención");
                                        alert.show();
                                        alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                                        deleteDialog.dismiss();
                                    }
                                }
                            });
                            deleteDialogView.findViewById(R.id.emergencia_falsa).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (connectedToWiFi3()) {
                                        try {
                                            database = FirebaseDatabase.getInstance();
                                            final DatabaseReference myRef1 = database.getReference("emergenciasAndroid").child(((Emergencia) marker2.getTag()).getidUsuario());
                                            myRef1.keepSynced(true);
                                            myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    if (this != null) {

                                                        try {
                                                            Emergencia value = dataSnapshot.getValue(Emergencia.class);
                                                            Integer i = Integer.parseInt(value.getestado().trim());
                                                            i++;
                                                            value.setEstado(i + "");
                                                            myRef1.setValue(value);
                                                            AlertDialog alert = createAlertDialog("Se reportó esta emergencia como falsa.", "Información");
                                                            alert.show();
                                                            alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                                                            deleteDialog.dismiss();


                                                        } catch (Throwable e) {

                                                        }


                                                    }

                                                }


                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });
                                        }catch (Throwable w){

                                        }
                                        deleteDialog.dismiss();
                                    }else{
                                        AlertDialog alert = createAlertDialog("No estas conectado a internet", "Atención");
                                        alert.show();
                                        alert.getWindow().setBackgroundDrawableResource(R.color.colorPurple);

                                        deleteDialog.dismiss();
                                    }
                                }
                            });

                            deleteDialog.show();

                        }
                    });
                    LatLng midLatLng = new LatLng(4.60,-74.065);
                    map.addMarker(new MarkerOptions().position(midLatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                createAlertDialog("Hubo un error con la sincronizacion de firebase", "Error Firebase");
            }
        });
    }


    public void enEstadoEmergencia() {
        if (locationManager != null && locationListener != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
            Log.e(TAG, "LOCATION UPDATES ACTIVADAS1");
        }
    }

    public void enEstadoNormal() {
        if (locationManager != null && locationListener != null) {

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.removeUpdates(locationListener);
            Log.e(TAG,"LOCATION UPDATES DESACTIVADAS2");
        }
    }


}
