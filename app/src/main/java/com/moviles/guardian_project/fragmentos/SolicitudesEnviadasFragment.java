package com.moviles.guardian_project.fragmentos;

import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.moviles.guardian_project.MainActivity;
import com.moviles.guardian_project.R;
import com.moviles.guardian_project.adapters.SolicitudesRecibidasAdapter;
import com.moviles.guardian_project.entities.Solicitud;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 30/03/2017.
 */

public class SolicitudesEnviadasFragment extends Fragment {


    private ListView listView;
    private List<Solicitud> numeros;
    private List<String> nombres;
    private List<String> ids;
    private String id= MainActivity.ID_DE_USUARIO_ACTUAL;
    private FirebaseDatabase database;
    private DatabaseReference myRef;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment

        return inflater.inflate(R.layout.fragment_solicitudes_realizadas, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        try {


            super.onViewCreated(view, savedInstanceState);

            database = FirebaseDatabase.getInstance();
            myRef = database.getInstance().getReference("solicitudes");
            myRef.keepSynced(true);


            listView = (ListView) view.findViewById(R.id.lista_sol_realic);
        }catch (Throwable e){
            AlertDialog alertDialog=createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo","Error");
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        }



    }


    public void actualizar(){

        try {
            nombres = new ArrayList<String>();
            numeros = new ArrayList<Solicitud>();
            ids = new ArrayList<String>();


            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(getActivity()!=null) {
                        try {

                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                Solicitud value = postSnapshot.getValue(Solicitud.class);

                                if(id.equals(value.getDe())) {

                                    nombres.add(value.getDe());
                                    numeros.add(value);
                                    ids.add(postSnapshot.getKey());
                                }


                            }

                            SolicitudesRecibidasAdapter items = new SolicitudesRecibidasAdapter(getActivity(), numeros);

                            listView.setAdapter(items);


                        } catch (Throwable e) {
                            AlertDialog alertDialog = createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo", "Error");
                            alertDialog.show();
                            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                        }
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Throwable e) {
            AlertDialog alertDialog = createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo", "Error");
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        }


    }

    @Override
    public void onResume(){
        try {
            super.onResume();
            actualizar();
        }catch (Throwable e){
            AlertDialog alertDialog=createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo","Error");
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        }
    }

    public AlertDialog createAlertDialog(String msg,String title){

        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        builder.setMessage(msg)
                .setTitle(title)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }
}
