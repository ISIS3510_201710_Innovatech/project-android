package com.moviles.guardian_project.fragmentos;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.moviles.guardian_project.MainActivity;
import com.moviles.guardian_project.activities.login.Login2;
import com.moviles.guardian_project.activities.protegidos.EditProtegidosActivity;
import com.moviles.guardian_project.activities.protegidos.ProtegidosDetailActivity;
import com.moviles.guardian_project.R;
import com.moviles.guardian_project.adapters.ProtegidoAdapter;
import com.moviles.guardian_project.entities.Protegido;
import com.moviles.guardian_project.entities.UserAnroid;
import com.moviles.guardian_project.entities.Usuario;
import com.moviles.guardian_project.entities.UsuarioProtegido;
import com.moviles.guardian_project.enums.TipoUsuario;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 19/02/2017.
 */

public class ListProtegidosFragment extends Fragment {

    private static final String TAG ="APLICACION" ;

    private ListView listView;
    private List<UsuarioProtegido> protegidos;
    private List<String>estados;
    private List<String> nombres;
    private List<String> ids;
    private String ID_USUARIO;
    private boolean bool=false;

    private FirebaseDatabase database;


    private int sincronizador;
    private SwipeRefreshLayout mSwipRefresLayout;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment

        return inflater.inflate(R.layout.fragment_protegidos, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);

        try {
            super.onViewCreated(view, savedInstanceState);
            database = FirebaseDatabase.getInstance();
            ID_USUARIO= MainActivity.ID_DE_USUARIO_ACTUAL;

            //myRef.keepSynced(true);

            database = FirebaseDatabase.getInstance();



            FloatingActionButton nuevoProtector2 = (FloatingActionButton) view.findViewById(R.id.nuevoProtegido);
            nuevoProtector2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    boolean connected = connectedToWiFi();
                    if (connected) {
                        Intent i = new Intent(getActivity(), EditProtegidosActivity.class);
                        i.putExtra("idUsuario", ID_USUARIO);
                        startActivity(i);
                    } else {
                        AlertDialog alertDialog=createAlertDialog("No estas conectado a internet, por lo tanto no puedes agregar contactos","Información");
                        alertDialog.show();
                        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                    }

                }
            });

            mSwipRefresLayout = (SwipeRefreshLayout)view.findViewById(R.id.activity_protegidos_swipe_refresh_layout);

            listView = (ListView) view.findViewById(R.id.lista_protegidos);

            mSwipRefresLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Log.e(TAG,"ENTRO ACA *****");
                    actualizar();
                }
            });

            }catch (Throwable e){
            AlertDialog alertDialog=createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo","Error");
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        }

    }

    public void actualizar(){
        if(getActivity()!=null) {
            try {
                Log.e(TAG, "Actualizando!!  ");


                nombres = new ArrayList<String>();
                protegidos = new ArrayList<UsuarioProtegido>();
                estados=new ArrayList<String>();
                ids = new ArrayList<String>();


                DatabaseReference myRef=database.getInstance().getReference("usuariosAndroid").child(ID_USUARIO).child("protegidos");
                myRef.keepSynced(true);
                myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(getActivity()!=null) {
                                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                    bool=false;

                                    UsuarioProtegido value = postSnapshot.getValue(UsuarioProtegido.class);
                                    nombres.add(value.getNombre());
                                    protegidos.add(value);
                                    ids.add(postSnapshot.getKey());
                                    ProtegidoAdapter items = new ProtegidoAdapter(getActivity(), protegidos,estados);
                                    //ArrayAdapter<String> items1=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,nombres);
                                    estados.add(value.getEstado());

                                    listView.setAdapter(items);
                                    sincronizador = 1;

                                    mSwipRefresLayout.setRefreshing(false);
                                    Log.e(TAG, "Terminó de Actualizar!!  ");

                                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                            UsuarioProtegido temp = protegidos.get(position);

                                            Intent i = new Intent(getActivity(), ProtegidosDetailActivity.class);
                                            i.putExtra("name", (String) nombres.get(position));
                                            i.putExtra("correo", temp.getCorreo());
                                            i.putExtra("telefono", temp.getTelefono() + "");
                                            i.putExtra("direccion", temp.getDireccion());
                                            i.putExtra("id", (String) ids.get(position));
                                            i.putExtra("idUsuario", ID_USUARIO);
                                            startActivity(i);
                                        }
                                    });
                                }



                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        mSwipRefresLayout.setRefreshing(false);
                    }
                });
            } catch (Throwable e) {
                mSwipRefresLayout.setRefreshing(false);
                AlertDialog alertDialog = createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo", "Error");
                alertDialog.show();
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
            }
        }
        mSwipRefresLayout.setRefreshing(false);
    }



    public AlertDialog createAlertDialog(String msg,String title){
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        builder.setMessage(msg)
                .setTitle(title)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }






    @Override
    public void onResume(){
        try {
            super.onResume();
            if(ID_USUARIO!=null && ID_USUARIO!="") {
                mSwipRefresLayout.setRefreshing(true);
                actualizar();
            }else{
                Intent i = new Intent(getActivity(), Login2.class);
                startActivity(i);
            }
        }catch (Throwable w){
            try{
                AlertDialog alertDialog=createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo","Error");
                alertDialog.show();
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
            }catch (Throwable e){

            }
        }


    }
    private boolean connectedToWiFi(){
        ConnectivityManager connManager=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

}
