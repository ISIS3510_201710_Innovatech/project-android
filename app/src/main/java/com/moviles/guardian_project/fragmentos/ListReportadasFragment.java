package com.moviles.guardian_project.fragmentos;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.moviles.guardian_project.R;
import com.moviles.guardian_project.activities.emergencias.atendidas.EmergenciasAtendidasActivity;
import com.moviles.guardian_project.activities.emergencias.reportadas.EmergenciasReportadasActivity;

import java.util.ArrayList;

/**
 * Created by Asus on 19/02/2017.hg
 */


public class ListReportadasFragment extends Fragment {
    private ListView listView;
    private ArrayList<String> numeros;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment

        return inflater.inflate(R.layout.fragment_reportadas, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);

        super.onViewCreated(view, savedInstanceState);
        listView=(ListView)view.findViewById(R.id.lista_reportadas);
        numeros=new ArrayList<String>();
        numeros.add("Emergencias reportadas");
        numeros.add("Emergencias atendidas");
        ArrayAdapter<String> items=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,numeros);
        listView.setAdapter(items);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               if(position==1) {
                   Intent i = new Intent(getActivity(), EmergenciasAtendidasActivity.class);
                   startActivity(i);
               }else{
                   Intent i = new Intent(getActivity(), EmergenciasReportadasActivity.class);
                   startActivity(i);
               }
            }
        });

    }
}
