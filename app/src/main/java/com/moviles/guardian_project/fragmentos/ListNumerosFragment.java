package com.moviles.guardian_project.fragmentos;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.moviles.guardian_project.R;
import com.moviles.guardian_project.adapters.NumerosAdapter;
import com.moviles.guardian_project.entities.TelefonosEmergencia;
import com.moviles.guardian_project.interfaces.BtnClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 19/02/2017.
 */

public class ListNumerosFragment extends Fragment {

    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;

    private ListView listView;
    private List<TelefonosEmergencia> numeros;
    private List<String> nombres;
    private List<String> ids;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private String numeroMarcado="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_numbers, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        try {
            super.onViewCreated(view, savedInstanceState);

            database = FirebaseDatabase.getInstance();
            //database.getInstance().setPersistenceEnabled(true);
            //FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            myRef = database.getInstance().getReference("telefonosEmergencia");
            myRef.keepSynced(true);
            listView = (ListView) view.findViewById(R.id.lista_numeros);
        }catch (Throwable e){
            AlertDialog alertDialog=createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevoo","Error");
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        }
    }


    private void actualizar(){
        try {
            nombres = new ArrayList<String>();
            numeros = new ArrayList<TelefonosEmergencia>();
            ids = new ArrayList<String>();
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(getActivity()!=null) {
                        try {
                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                TelefonosEmergencia value = postSnapshot.getValue(TelefonosEmergencia.class);
                                nombres.add(value.getNombre());
                                numeros.add(value);
                                ids.add(postSnapshot.getKey());
                            }
                            NumerosAdapter items = new NumerosAdapter(getActivity(), numeros, new BtnClickListener() {
                                @Override
                                public void onBtnClick(int position) {
                                    int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
                                    numeroMarcado="tel:"+numeros.get(position).getTelefonos();
                                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL_PHONE);
                                    } else {
                                        numeroMarcado="tel:"+numeros.get(position).getTelefonos();
                                        callPhone(numeroMarcado);
                                    }
                                }
                            });
                    listView.setAdapter(items);
                    } catch (Throwable e) {
                            AlertDialog alertDialog = createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo.", "Error");
                            alertDialog.show();
                            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    try {
                        AlertDialog alertDialog = createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo", "Error");
                        alertDialog.show();
                        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                    }catch (Throwable er){

                    }
                }
            });
            } catch (Throwable e) {
                try {
                    AlertDialog alertDialog = createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo", "Error");
                    alertDialog.show();
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                }catch (Throwable er){

                }
            }


    }

    @Override
    public void onResume(){
        try {
            super.onResume();
            actualizar();
        }catch (Throwable e){
            try {
                AlertDialog alertDialog = createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo", "Error");
                alertDialog.show();
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
            }catch (Throwable er){

            }
        }
    }

    public AlertDialog createAlertDialog(String msg,String title){
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setMessage(msg)
                .setTitle(title)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callPhone(numeroMarcado);
                }
            }
        }
    }

    private void callPhone(String tag){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(tag));
        startActivity(callIntent);
    }

}
