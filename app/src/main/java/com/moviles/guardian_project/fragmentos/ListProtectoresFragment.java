package com.moviles.guardian_project.fragmentos;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.moviles.guardian_project.MainActivity;
import com.moviles.guardian_project.R;
import com.moviles.guardian_project.activities.login.Login2;
import com.moviles.guardian_project.activities.protectores.ProtectoresDetailActivity;
import com.moviles.guardian_project.activities.protectores.ProtectoresEditActivity;
import com.moviles.guardian_project.adapters.ProtectorAdapter;
import com.moviles.guardian_project.entities.Protegido;
import com.moviles.guardian_project.entities.UsuarioProtegido;



import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 19/02/2017.
 */


public class ListProtectoresFragment extends Fragment {

    private static final String TAG ="APLICACION" ;

    ListView listView;
    ArrayList<UsuarioProtegido> usuarios;
    List<UsuarioProtegido> protegidos;
    List<String> nombres;
    List<String> ids;
    String ID_USUARIO;

    private FirebaseDatabase database;
    private DatabaseReference myRef;

    private int sincronizador;

    private SwipeRefreshLayout mSwipRefresLayout;
    private ProgressDialog loadingDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment

        return inflater.inflate(R.layout.fragment_protectores, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);


        super.onViewCreated(view, savedInstanceState);
        ID_USUARIO= MainActivity.ID_DE_USUARIO_ACTUAL;
        database = FirebaseDatabase.getInstance();
        myRef = database.getInstance().getReference("usuariosAndroid").child(ID_USUARIO).child("protectores");
        myRef.keepSynced(true);

        /*Button nuevoProtector = (Button) view.findViewById(R.id.nuevoProtector);
        nuevoProtector.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(),ProtectoresEditActivity.class);
                i.putExtra("idUsuario", ID_USUARIO);
                startActivity(i);
            }
        });*/

        FloatingActionButton nuevoProtector2 = (FloatingActionButton)  view.findViewById(R.id.nuevoProtector2);
        nuevoProtector2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

            boolean connected = connectedToWiFi();
            if (connected) {
                Intent i = new Intent(getActivity(),ProtectoresEditActivity.class);
                i.putExtra("idUsuario", ID_USUARIO);
                startActivity(i);
            } else {
                AlertDialog alertDialog=createAlertDialog("No estas conectado a internet, por lo tanto no puedes agregar contactos. Por favor conéctate y vuelve a intentarlo","Información");
                alertDialog.show();
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
            }


            }
        });


       mSwipRefresLayout = (SwipeRefreshLayout)view.findViewById(R.id.activity_protectores_swipe_refresh_layout);

        listView=(ListView)view.findViewById(R.id.lista_protectores);

       mSwipRefresLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e(TAG,"ENTRO ACA *****");
                //getProtectores();
                actualizar();
            }
        });



    }


    public void actualizar(){
        if(getActivity()!=null) {
            try {
                Log.e(TAG, "Actualizando!!  ");

                nombres = new ArrayList();
                usuarios = new ArrayList<UsuarioProtegido>();
                ids = new ArrayList<String>();

                myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                            UsuarioProtegido value = postSnapshot.getValue(UsuarioProtegido.class);
                            //  Usuario value = postSnapshot.getValue(Usuario.class);
                            Log.d(TAG, "Value is: " + value.getNombre());
                            nombres.add(value.getNombre());
                            usuarios.add(value);
                            ids.add(postSnapshot.getKey());

                        }


                        ProtectorAdapter items = new ProtectorAdapter(getActivity(), usuarios);

                        listView.setAdapter(items);

                        mSwipRefresLayout.setRefreshing(false);

                        Log.e(TAG, "Terminó de Actualizar!!  ");

                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                UsuarioProtegido temp = usuarios.get(position);

                                Intent i = new Intent(getActivity(), ProtectoresDetailActivity.class);
                                i.putExtra("name", (String) nombres.get(position));
                                i.putExtra("correo", temp.getCorreo());
                                i.putExtra("telefono", temp.getTelefono() + "");
                                i.putExtra("direccion", temp.getDireccion());
                                i.putExtra("id", (String) ids.get(position));
                                i.putExtra("idUsuario", ID_USUARIO);

                                startActivity(i);
                            }
                        });


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        mSwipRefresLayout.setRefreshing(false);
                    }
                });
            }catch (Throwable e){
                try{
                    mSwipRefresLayout.setRefreshing(false);

                }catch (Throwable ee){
                    mSwipRefresLayout.setRefreshing(false);
                    AlertDialog alertDialog = createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo", "Error");
                    alertDialog.show();
                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                }
            }
        }
    }

    public AlertDialog createAlertDialog(String msg,String title){
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        builder.setMessage(msg)
                .setTitle(title)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }





    @Override
    public void onResume(){
        try {
            super.onResume();
            if(ID_USUARIO!=null && ID_USUARIO!="") {
                mSwipRefresLayout.setRefreshing(true);
                //getProtectores();
                actualizar();
            }else{
                Intent i = new Intent(getActivity(), Login2.class);
                startActivity(i);
            }
        }catch (Throwable w){
            try{
                AlertDialog alertDialog=createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo","Error");
                alertDialog.show();
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
            }catch (Throwable e){

            }
        }


    }

    private boolean connectedToWiFi(){
        ConnectivityManager connManager=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }
}
