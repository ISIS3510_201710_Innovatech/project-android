package com.moviles.guardian_project.fragmentos;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.moviles.guardian_project.MainActivity;
import com.moviles.guardian_project.R;
import com.moviles.guardian_project.adapters.MiasSolicitudesAdapter;
import com.moviles.guardian_project.entities.Protegido;
import com.moviles.guardian_project.entities.Solicitud;
import com.moviles.guardian_project.entities.UsuarioProtegido;
import com.moviles.guardian_project.interfaces.BtnClickListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Asus on 30/03/2017.
 */

public class SolicitudesRecibidasFragment extends Fragment {


    private ListView listView;
    private List<Solicitud> numeros;
    private List<String> nombres;
    private List<String> ids;
    private String id=MainActivity.ID_DE_USUARIO_ACTUAL;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private DatabaseReference protectoresRef;
    private DatabaseReference protectores;
    private DatabaseReference solicitudes;
    private String telefonoDe;
    private String telefonoPara;
    private String direccionDe;
    private String direccionPara;
    private String estadoDe;
    private String estadoPara;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment

        return inflater.inflate(R.layout.fragment_mias_solicitudes, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        try {


            super.onViewCreated(view, savedInstanceState);

            database = FirebaseDatabase.getInstance();
            myRef = database.getInstance().getReference("solicitudes");
            myRef.keepSynced(true);


            listView = (ListView) view.findViewById(R.id.lista_mias_solicitudes);
        }catch (Throwable e){
            AlertDialog alertDialog=createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo","Error");
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        }



    }


    public void actualizar(){

        try {
            nombres = new ArrayList<String>();
            numeros = new ArrayList<Solicitud>();
            ids = new ArrayList<String>();


            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(getActivity()!=null) {
                        try {

                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                Solicitud value = postSnapshot.getValue(Solicitud.class);

                                if(id.equals(value.getPara())) {

                                    nombres.add(value.getDe());
                                    numeros.add(value);
                                    ids.add(postSnapshot.getKey());
                                }


                            }

                            MiasSolicitudesAdapter items = new MiasSolicitudesAdapter(getActivity(), numeros, new BtnClickListener() {
                                @Override
                                public void onBtnClick(int position) {
                                    if (connectedToWiFi()) {
                                        //acepto
                                        final Solicitud solicitud = numeros.get(position);
                                        if (solicitud.getTipo().equals("Protegido")) {
                                            //en mis protectores sale el y yo en sus protegidos
                                            protectoresRef = database.getReference("usuariosAndroid").child(solicitud.getDe()).child("protegidos").child(solicitud.getPara());
                                            protectores = database.getReference("usuariosAndroid").child(solicitud.getPara());
                                            protectores.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    if (this != null) {
                                                        Protegido protegido = dataSnapshot.getValue(Protegido.class);
                                                        UsuarioProtegido up = new UsuarioProtegido(protegido);
                                                        String llave = solicitud.getPara();
                                                        HashMap<String, Object> protector = new HashMap<String, Object>();
                                                        protector.put(llave, up.toMap());
                                                        protectoresRef.setValue(up.toMap());
                                                        protectoresRef = database.getReference("usuariosAndroid").child(solicitud.getPara()).child("protectores").child(solicitud.getDe());
                                                        protectores = database.getReference("usuariosAndroid").child(solicitud.getDe());
                                                        protectores.addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                                if (this != null) {
                                                                    Protegido protegido = dataSnapshot.getValue(Protegido.class);
                                                                    UsuarioProtegido up = new UsuarioProtegido(protegido);
                                                                    String llave = solicitud.getPara();
                                                                    HashMap<String, Object> protector = new HashMap<String, Object>();
                                                                    protector.put(llave, up.toMap());
                                                                    protectoresRef.setValue(up.toMap());
                                                                    solicitudes = database.getReference("solicitudes");
                                                                    solicitudes.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                                            if (this != null) {
                                                                                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                                                                    Solicitud value = postSnapshot.getValue(Solicitud.class);
                                                                                    if (value.getTipo().equals(solicitud.getTipo()) && value.getDe().equals(solicitud.getDe())) {

                                                                                        DatabaseReference r = database.getReference("solicitudes").child(postSnapshot.getKey());
                                                                                        r.setValue(null);
                                                                                    }
                                                                                }
                                                                                AlertDialog alertDialog = createAlertDialog("Se aceptó la solicitud de " + solicitud.getDeCorreo() + " satisfactoriamente", "Información");
                                                                                alertDialog.show();
                                                                                alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                                                                                FragmentManager fm=getFragmentManager();
                                                                                fm.beginTransaction().replace(R.id.content_frame,new SolicitudesRecibidasFragment()).commit();
                                                                                MainActivity.vista=5;
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onCancelled(DatabaseError databaseError) {

                                                                        }
                                                                    });
                                                                }
                                                            }

                                                            @Override
                                                            public void onCancelled(DatabaseError databaseError) {

                                                            }
                                                        });
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });

                                        } else {

                                            protectoresRef = database.getReference("usuariosAndroid").child(solicitud.getDe()).child("protectores").child(solicitud.getPara());
                                            protectores = database.getReference("usuariosAndroid").child(solicitud.getPara());
                                            protectores.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    if (this != null) {
                                                        Protegido protegido = dataSnapshot.getValue(Protegido.class);
                                                        UsuarioProtegido up = new UsuarioProtegido(protegido);
                                                        String llave = solicitud.getPara();
                                                        HashMap<String, Object> protector = new HashMap<String, Object>();
                                                        protector.put(llave, up.toMap());
                                                        protectoresRef.setValue(up.toMap());
                                                        protectoresRef = database.getReference("usuariosAndroid").child(solicitud.getPara()).child("protegidos").child(solicitud.getDe());
                                                        protectores = database.getReference("usuariosAndroid").child(solicitud.getDe());
                                                        protectores.addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                                if (this != null) {
                                                                    Protegido protegido = dataSnapshot.getValue(Protegido.class);
                                                                    UsuarioProtegido up = new UsuarioProtegido(protegido);
                                                                    String llave = solicitud.getPara();
                                                                    HashMap<String, Object> protector = new HashMap<String, Object>();
                                                                    protector.put(llave, up.toMap());
                                                                    protectoresRef.setValue(up.toMap());
                                                                    solicitudes = database.getReference("solicitudes");
                                                                    solicitudes.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                                            if (this != null) {
                                                                                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                                                                    Solicitud value = postSnapshot.getValue(Solicitud.class);
                                                                                    if (value.getTipo().equals(solicitud.getTipo()) && value.getDe().equals(solicitud.getDe())) {

                                                                                        DatabaseReference r = database.getReference("solicitudes").child(postSnapshot.getKey());
                                                                                        r.setValue(null);
                                                                                    }
                                                                                }
                                                                                AlertDialog alertDialog = createAlertDialog("Se aceptó la solicitud de " + solicitud.getDeCorreo() + " satisfactoriamente", "Información");
                                                                                alertDialog.show();
                                                                                alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                                                                                FragmentManager fm=getFragmentManager();
                                                                                fm.beginTransaction().replace(R.id.content_frame,new SolicitudesRecibidasFragment()).commit();
                                                                                MainActivity.vista=5;
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onCancelled(DatabaseError databaseError) {

                                                                        }
                                                                    });
                                                                }
                                                            }

                                                            @Override
                                                            public void onCancelled(DatabaseError databaseError) {

                                                            }
                                                        });
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });

                                        }

                                    }else {
                                        AlertDialog alertDialog = createAlertDialog("No está conectado a internet. Por favor conéctese y vuelva aintentarlo", "Error");
                                        alertDialog.show();
                                        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                                    }
                                }
                            }, new BtnClickListener() {
                                @Override
                                public void onBtnClick(int position) {
                                    //rechazo
                                    if (connectedToWiFi()) {
                                        final Solicitud solicitud = numeros.get(position);
                                        solicitudes = database.getReference("solicitudes");
                                        solicitudes.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                if (this != null) {
                                                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                                        Solicitud value = postSnapshot.getValue(Solicitud.class);
                                                        if (value.getTipo().equals(solicitud.getTipo()) && value.getDe().equals(solicitud.getDe())) {

                                                            DatabaseReference r = database.getReference("solicitudes").child(postSnapshot.getKey());
                                                            r.setValue(null);
                                                        }
                                                    }
                                                    AlertDialog alertDialog = createAlertDialog("Se rechazó la solicitud de " + solicitud.getDeCorreo(), "Información");
                                                    alertDialog.show();
                                                    alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                                                    FragmentManager fm=getFragmentManager();
                                                    fm.beginTransaction().replace(R.id.content_frame,new SolicitudesRecibidasFragment()).commit();
                                                    MainActivity.vista=5;
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                                    }else{
                                        AlertDialog alertDialog = createAlertDialog("No está conectado a internet. Por favor conéctese y vuelva aintentarlo", "Error");
                                        alertDialog.show();
                                        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);

                                    }
                                }
                            });

                            listView.setAdapter(items);


                        } catch (Throwable e) {
                            AlertDialog alertDialog = createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo", "Error");
                            alertDialog.show();
                            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
                        }
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }

            });
        } catch (Throwable e) {
            AlertDialog alertDialog = createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo", "Error");
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        }


    }

    @Override
    public void onResume(){
        try {
            super.onResume();
            actualizar();
        }catch (Throwable e){
            AlertDialog alertDialog=createAlertDialog("Ocurrió algo inesperado, por favor inténtelo de nuevo","Error");
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorPurple);
        }
    }

    private boolean connectedToWiFi(){
        ConnectivityManager connManager=(ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    public AlertDialog createAlertDialog(String msg,String title){

        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        builder.setMessage(msg)
                .setTitle(title)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }
}
