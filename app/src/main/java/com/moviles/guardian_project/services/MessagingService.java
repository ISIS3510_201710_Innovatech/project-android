package com.moviles.guardian_project.services;

import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.moviles.guardian_project.MainActivity;
import com.moviles.guardian_project.persistance.TokenFirebase;
import com.moviles.guardian_project.persistance.TokenFirebaseDB;

/**
 * Created by Asus on 21/04/2017.
 */

public class MessagingService extends FirebaseInstanceIdService {
    public TokenFirebaseDB databaseHelper;
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("######################", "Refreshed token: " + refreshedToken + " #####");
        if(MainActivity.yo!=null) {

            FirebaseDatabase database;

            DatabaseReference myRef;
            database = FirebaseDatabase.getInstance();
            myRef = database.getReference("usuariosAndroid").child(MainActivity.ID_DE_USUARIO_ACTUAL);

            myRef.child("token").setValue(refreshedToken);
            MainActivity.tokenDesactualizado=false;
            // If you want to send messages to this application instance or
            // manage this apps subscriptions on the server side, send the
            // Instance ID token to your app server.
            //sendRegistrationToServer(refreshedToken);
        }else{
            databaseHelper = new TokenFirebaseDB(this);
            TokenFirebase token=new TokenFirebase(0,refreshedToken);
            databaseHelper.addFake(token);
            MainActivity.tokenDesactualizado=true;


        }
    }
}
