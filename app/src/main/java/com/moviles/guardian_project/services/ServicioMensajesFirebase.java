package com.moviles.guardian_project.services;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.moviles.guardian_project.R;

public class ServicioMensajesFirebase extends FirebaseMessagingService {
    public static int id_msg=0;
    public ServicioMensajesFirebase() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        Log.e("%%%%%%%%%%%%%%", "From: " + remoteMessage.getFrom());
        Log.e("%%%%%%%%%%%", "Notification Message Body: " + remoteMessage.getNotification().getBody());
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

// Vibrate for 300 milliseconds
        v.vibrate(600);
        String color = remoteMessage.getNotification().getColor();
        String body = remoteMessage.getNotification().getBody();
        String title = remoteMessage.getNotification().getTitle();
        String icon = remoteMessage.getNotification().getIcon();
        NotificationCompat.Builder builder = new  NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)

                .setContentText(body);

        NotificationManager manager = (NotificationManager)     getSystemService(NOTIFICATION_SERVICE);
        manager.notify(id_msg, builder.build());
        id_msg++;
    }
}
