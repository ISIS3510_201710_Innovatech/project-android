package com.moviles.guardian_project.services;

import android.Manifest;
import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.moviles.guardian_project.MainActivity;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class GPSIntentService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    public static final String GET_POSITION = "com.moviles.guardian_project.services.action.GETPOS";


    public GPSIntentService() {
        super("GPSIntentService");
    }





    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (GET_POSITION.equals(action)) {
                handleGPSAction();
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleGPSAction() {
        LocationManager mlockManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION )== PackageManager.PERMISSION_GRANTED
               &&   ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION )== PackageManager.PERMISSION_GRANTED) {

            Log.e("APLICACION", "ENTRO AL INTENT");

            Location mLastLocation = mlockManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (mLastLocation != null) {
                Intent result = new Intent(MainActivity.GPS_FILTER);
                result.putExtra("position", mLastLocation.getLatitude());
                result.putExtra("longitud", mLastLocation.getLongitude());
                sendBroadcast(result);
                Log.e("APLICACION", "ENTRO AL INTENT  " + result.putExtra("position", mLastLocation.getLatitude()));
            } else {
                Log.e("APLICACION", "ENTRO AL INTENT null ");

                Intent result = new Intent(MainActivity.GPS_FILTER);
                result.putExtra("position", "Not found");
                result.putExtra("longitud", "Not found");
                sendBroadcast(result);
            }
        }

    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */


}
