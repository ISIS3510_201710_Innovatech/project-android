package com.moviles.guardian_project.services;

import android.Manifest;

import android.app.IntentService;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.util.Log;

import android.view.MotionEvent;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import com.moviles.guardian_project.entities.EmergenciaAndroid;
import com.moviles.guardian_project.entities.EmergenciaEnviadaFirebase;
import com.moviles.guardian_project.entities.MensajeFirebase;
import com.moviles.guardian_project.entities.UserAnroid;
import com.moviles.guardian_project.entities.UsuarioProtegido;
import com.moviles.guardian_project.rest.RestClient;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Johan on 25/04/2017.
 */

public class Acelerometro extends IntentService implements SensorEventListener, View.OnTouchListener {

    private static final float SHAKE_THRESHOLD_GRAVITY = 1.8F;
    private static final int SHAKE_SLOP_TIME_MS = 400;
    private static final int SHAKE_COUNT_RESET_TIME_MS = 1500;
    private static final int DOUBLE_CLICK_INTERVAL = 500;
    private static final int LONG_HOLD_TIMEOUT = 700;
    private long mShakeTimeSTAMP;
    private int mShakeCount;
    private SensorManager mSensorManager;
    private Sensor mAcc;
    private Map<String, Object> childUpdates;
    private long thisTouchTime;
    private long previousTouchTime = 0;
    private long buttonHeldTime;
    private boolean clickHandled = false;
    private int taps = 0;

    private Double longitud = 4.604;
    private Double latitud = -74.04;
    private FirebaseDatabase database;
    private DatabaseReference myRefEmergency;

    private Acelerometro servicio;
    private String USER_ID;
    private String mensajeSolicitud = "Ayuda, estoy en emergencia";


    public Acelerometro() {
        super("ACELEROMETRO_SERVICE");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            USER_ID = intent.getStringExtra("userId");
            Log.e("ACELEROMETRO", "ID DE USUARIO " + USER_ID);
        }
        // String password = intent.getStringExtra("password");

    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        new SensorEventLoggerTask().execute(event);
        servicio = this;
        //mSensorManager.unregisterListener(this);
        //stopSelf();

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        new TapEventLoggerTask().execute(motionEvent);

        return false;
    }

    private class SensorEventLoggerTask extends AsyncTask<SensorEvent, Void, Void> {

        @Override
        protected Void doInBackground(SensorEvent... events) {

            //Log.e("ACELEROMETRO","ENTRO A BACKGROUND");
            SensorEvent event = (SensorEvent) events[0];
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            float gx = x / SensorManager.GRAVITY_EARTH;
            float gy = y / SensorManager.GRAVITY_EARTH;
            float gz = z / SensorManager.GRAVITY_EARTH;
            double gForce = Math.sqrt(gx * gx + gy * gy + gz * gz);
            if (gForce > SHAKE_THRESHOLD_GRAVITY) {
                final long now = System.currentTimeMillis();
                if (mShakeTimeSTAMP + SHAKE_SLOP_TIME_MS > now) {
                    return null;
                }
                if (mShakeTimeSTAMP + SHAKE_COUNT_RESET_TIME_MS < now) {
                    mShakeCount = 0;
                    Log.e("ACELEROMETRO", "REINICIANDO CUENTA");
                }
                mShakeTimeSTAMP = now;
                mShakeCount++;
                Log.e("ACELEROMETRO", " CUENTA = " + mShakeCount + "- " + gForce);
                if (mShakeCount >= 5) {
                    mShakeCount = 0;
                    Log.e("ACELEROMETRO", "ENVIANDO ACCION ACELEROMETRO");

                    LocationManager locationManager = (LocationManager) servicio.getSystemService(Context.LOCATION_SERVICE);
                    if (ActivityCompat.checkSelfPermission(servicio, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(servicio, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return null;
                    }
                    Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if(location==null)location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if(location!=null){
                        latitud=location.getLatitude();
                        longitud=location.getLongitude();
                    }


                    reportarEmergencia();
                    mSensorManager.unregisterListener(servicio);
                    servicio.stopSelf();
                }
            }
            return null;
        }
    }

    @Override
    public void onDestroy() {
        mSensorManager.unregisterListener(this);
        stopSelf();
        Log.e("ACELEROMETRO","Service detenido");
    }


    private class TapEventLoggerTask extends AsyncTask<MotionEvent, Void, Void>{

        @Override
        protected Void doInBackground(MotionEvent... events) {

            //Log.e("ACELEROMETRO","ENTRO A BACKGROUND");
            MotionEvent event=(MotionEvent)events[0];
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    thisTouchTime = System.currentTimeMillis();
                    if (thisTouchTime - previousTouchTime <= DOUBLE_CLICK_INTERVAL) {
                        // double click detected
                        Log.e("TAPS","INICIANDO TAP");
                        clickHandled = true;
                    } else {
                        taps=0;
                        // defer event handling until later
                        clickHandled = false;
                    }
                    previousTouchTime = thisTouchTime;
                    break;
                case MotionEvent.ACTION_UP:
                    if (clickHandled) {
                        buttonHeldTime = System.currentTimeMillis() - thisTouchTime;
                        if (buttonHeldTime < LONG_HOLD_TIMEOUT) {
                            taps++;
                            clickHandled=false;
                            if(taps>=5){
                                taps=0;
                                Log.e("TAPS","ENVIANDO EMERGENCIA CON TAPS");

                            }
                        }
                    }

                    break;
            }

            //Log.e("ACELEROMETRO","ENVIANDO ACCION ACELEROMETRO");

            return null;
        }
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(intent!= null) {
            USER_ID = intent.getStringExtra("userId");
            Log.e("ACELEROMETRO", "ID DE USUARIO " + USER_ID);
        }


        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAcc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mSensorManager.registerListener(this, mAcc, SensorManager.SENSOR_DELAY_NORMAL);
        Log.e("SERVICE ","Entro al service");

       return START_STICKY;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private boolean connectedToWiFi3() {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    private void sendSMS(String phoneNumber, String message) {
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, null, null);
    }

public void reportarEmergencia(){
    if (connectedToWiFi3()) {
        //your business logic

        //estadoEmergencia = true;
        //enEstadoEmergencia();

        database = FirebaseDatabase.getInstance();
        myRefEmergency = database.getReference("emergenciasAndroid");
        myRefEmergency.keepSynced(true);
        EmergenciaAndroid emergenciaAndroid = new EmergenciaAndroid("0", (new Date()).toString(), USER_ID, latitud, longitud, mensajeSolicitud, "Salud");
        HashMap<String, Object> protector = new HashMap<String, Object>();
        protector.put(USER_ID, emergenciaAndroid.toMap());

        childUpdates = protector;
        myRefEmergency.updateChildren(childUpdates);

        database = FirebaseDatabase.getInstance();
        myRefEmergency = database.getReference("usuariosAndroid").child(USER_ID).child("emergenciasReportadas");
        HashMap<String, Object> protector2 = new HashMap<String, Object>();
        protector2.put(myRefEmergency.push().getKey(), emergenciaAndroid.toMap());
        childUpdates = protector2;
        myRefEmergency.updateChildren(childUpdates);


        database = FirebaseDatabase.getInstance();
        myRefEmergency = database.getReference("usuariosAndroid").child(USER_ID).child("estado");
        myRefEmergency.setValue("Emergencia");

        enviarNotificacion("Salud", mensajeSolicitud);

    } else {
        //sendSMS("3214869283","Mensaje Guardian: Salud - "+mensaje);
        EmergenciaAndroid emergenciaAndroid = new EmergenciaAndroid("emergencia", (new Date()).toString(), USER_ID, latitud, longitud, mensajeSolicitud, "Salud");

        database = FirebaseDatabase.getInstance();
        myRefEmergency = database.getReference("usuariosAndroid").child(USER_ID).child("emergenciasReportadas");
        HashMap<String, Object> protector2 = new HashMap<String, Object>();
        protector2.put(myRefEmergency.push().getKey(), emergenciaAndroid.toMap());
        childUpdates = protector2;
        myRefEmergency.updateChildren(childUpdates);

        database = FirebaseDatabase.getInstance();
        DatabaseReference myRef1 = database.getReference("usuariosAndroid").child(USER_ID).child("protectores");
        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (this != null) {

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        try {
                            UsuarioProtegido value = postSnapshot.getValue(UsuarioProtegido.class);
                            sendSMS(value.getTelefono(), "Mensaje Guardian: Salud - " + mensajeSolicitud);

                        } catch (Throwable e) {

                        }

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
}

    public void actualizarEstadoAPeligro(){
        database = FirebaseDatabase.getInstance();
        DatabaseReference myRef1=database.getReference("usuariosAndroid");
        myRef1.keepSynced(true);
        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (this != null) {

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        try {
                            UserAnroid value = postSnapshot.getValue(UserAnroid.class);
                            database = FirebaseDatabase.getInstance();
                            HashMap<String,Object>mapa=(new HashMap<String, Object>());
                            mapa.put("estado","Emergencia");
                            for(String usuario:value.getProtegidos().keySet()) {
                                if (usuario.equals(USER_ID)) {
                                    Log.e("0090908080808080800", database.getReference("usuariosAndroid").child(value.getId()).child("protegidos").child(USER_ID).getKey());

                                    database.getReference("usuariosAndroid").child(value.getId()).child("protegidos").child(USER_ID).updateChildren(mapa);


                                }
                            }


                        }catch (Throwable e){

                        }

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void enviarNotificacion(String t, String m) {
        actualizarEstadoAPeligro();



        database = FirebaseDatabase.getInstance();
        DatabaseReference myRef1 = database.getReference("usuariosAndroid").child(USER_ID).child("protectores");
        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (this != null) {

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        try {
                            UsuarioProtegido value = postSnapshot.getValue(UsuarioProtegido.class);
                            database = FirebaseDatabase.getInstance();
                            DatabaseReference myRef2 = database.getReference("usuariosAndroid").child(value.getId());
                            myRef2.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (this != null) {

                                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                            if (postSnapshot.getKey().equals("token")) {

                                                try {
                                                    Log.e("1121212121212", postSnapshot.toString());
                                                    EmergenciaEnviadaFirebase emergenciaEnviadaFirebase = new EmergenciaEnviadaFirebase("Urgente" + " - " + "Ayuda, estoy en emergencia", postSnapshot.getValue(String.class));
                                                    Call<MensajeFirebase> call = RestClient.getInstance().getApiService().enviarNotificacion(emergenciaEnviadaFirebase);

                                                    call.enqueue(new Callback<MensajeFirebase>() {
                                                        @Override
                                                        public void onResponse(Call<MensajeFirebase> call, Response<MensajeFirebase> response) {
                                                            Log.e("111111111111", response.code() + "");
                                        /*Log.e("5555555555555",call.request().toString());
                                        Log.e("111111111111", response.code()+"");
                                        Log.e("888888888",call.request().headers().toString());
                                        try {
                                            final Buffer buffer = new Buffer();

                                                call.request().body().writeTo(buffer);

                                            Log.e("5555555555555",buffer.readUtf8());
                                        } catch (final IOException e) {
                                            Log.e("5555555555555", "did not work");
                                        }
                                    */
                                                        }

                                                        @Override
                                                        public void onFailure(Call<MensajeFirebase> call, Throwable t) {
                                                            Log.e("5555555555555", call.request().toString());
                                                        }
                                                    });
                                                } catch (Throwable e) {

                                                }
                                            }

                                        }

                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                        } catch (Throwable e) {

                        }

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        database = FirebaseDatabase.getInstance();
        myRef1 = database.getReference("usuariosAndroid");
        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (this != null) {

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        try {
                            EmergenciaEnviadaFirebase emergenciaEnviadaFirebase = new EmergenciaEnviadaFirebase("Urgente" + " - " + mensajeSolicitud, postSnapshot.getValue(UserAnroid.class).getToken());
                            Call<MensajeFirebase> call = RestClient.getInstance().getApiService().enviarNotificacion(emergenciaEnviadaFirebase);

                            call.enqueue(new Callback<MensajeFirebase>() {
                                @Override
                                public void onResponse(Call<MensajeFirebase> call, Response<MensajeFirebase> response) {
                                    Log.e("111111111111", response.code() + "");
                                        /*Log.e("5555555555555",call.request().toString());
                                        Log.e("111111111111", response.code()+"");
                                        Log.e("888888888",call.request().headers().toString());
                                        try {
                                            final Buffer buffer = new Buffer();

                                                call.request().body().writeTo(buffer);

                                            Log.e("5555555555555",buffer.readUtf8());
                                        } catch (final IOException e) {
                                            Log.e("5555555555555", "did not work");
                                        }
                                    */
                                }

                                @Override
                                public void onFailure(Call<MensajeFirebase> call, Throwable t) {
                                    Log.e("5555555555555", call.request().toString());
                                }
                            });
                        } catch (Throwable e) {

                        }

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


}
